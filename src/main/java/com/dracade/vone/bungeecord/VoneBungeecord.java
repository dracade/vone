package com.dracade.vone.bungeecord;

import com.dracade.vone.bukkit.api.VoneCore;
import com.dracade.vone.core.api.backend.VoneDatabase;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.api.plugin.PluginClassloader;
import net.md_5.bungee.api.scheduler.GroupedThreadFactory;

public class VoneBungeecord extends Plugin {

    private static VoneBungeecord instance;

    /**
     * Retrieve the vone plugin.
     *
     * @return VoneBungeecord instance
     */
    public static VoneBungeecord plugin() {
        return VoneBungeecord.instance;
    }

    private VoneCore core;

    /**
     * Create a new VoneBungeecord instance.
     */
    public VoneBungeecord() {
        this.core = VoneCore.getInstance();
    }

    @Override
    public void onEnable() {
        VoneBungeecord.instance = this;

        // VoneDatabase db = new VoneDatabase(PluginClassloader.getSystemClassLoader());
    }

    /**
     * Retrieve the core helper methods for vone.
     *
     * @return
     */
    public final VoneCore getCore() {
        return this.core;
    }

}
