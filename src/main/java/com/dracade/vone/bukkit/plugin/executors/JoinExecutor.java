package com.dracade.vone.bukkit.plugin.executors;

import com.dracade.vone.bukkit.VoneBukkit;
import com.dracade.vone.bukkit.api.game.Playable;
import com.dracade.vone.bukkit.api.game.Minigame;
import com.dracade.vone.bukkit.api.system.VoneExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Locale;
import java.util.Optional;
import java.util.UUID;

public class JoinExecutor implements VoneExecutor {

    @Override
    public boolean execute(CommandSender sender, String[] args) throws ArrayIndexOutOfBoundsException {
        final Locale locale;

        if (sender instanceof Player) {
            locale = VoneBukkit.plugin().getPlayerLocale(((Player) sender));
        } else {
            locale = VoneBukkit.plugin().getDefaultLocale();
        }

        if (sender.hasPermission("vone.join")) {
            if (sender instanceof Player) {
                Player player = ((Player) sender);
                if (VoneBukkit.plugin().registry().isActive(player)) {
                    sender.sendMessage(VoneBukkit.plugin().i18n().translateDirect(locale, "messages.errors.quit-minigame-required"));
                } else {
                    try {
                        UUID id = UUID.fromString(args[0]);
                        Optional<Minigame> minigame = VoneBukkit.plugin().registry().getMinigames().stream().filter(m -> m.getUniqueId().equals(id)).findFirst();

                        if (minigame.isPresent()) {
                            if (minigame.get() instanceof Playable) {
                                ((Playable) minigame.get()).addPlayer(player.getUniqueId());
                                sender.sendMessage(VoneBukkit.plugin().i18n().translateDirect(locale, "messages.informative.joining-game"));
                            } else {
                                sender.sendMessage(VoneBukkit.plugin().i18n().translateDirect(locale, "messages.errors.not-joinable"));
                            }
                        } else {
                            sender.sendMessage(VoneBukkit.plugin().i18n().translateDirect(locale, "messages.errors.invalid-minigame-id"));
                        }
                    } catch (IllegalArgumentException e) {
                        sender.sendMessage(VoneBukkit.plugin().i18n().translateDirect(locale, "messages.errors.invalid-minigame-id"));
                    }
                }
            } else {
                sender.sendMessage(VoneBukkit.plugin().i18n().translateDirect(locale, "messages.errors.player-only-command"));
            }
        } else {
            sender.sendMessage(VoneBukkit.plugin().i18n().translateDirect(locale, "messages.errors.no-permission"));
        }

        return true;
    }

}
