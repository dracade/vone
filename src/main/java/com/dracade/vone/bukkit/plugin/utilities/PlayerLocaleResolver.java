package com.dracade.vone.bukkit.plugin.utilities;

import com.blackypaw.mc.i18n.LocaleResolver;
import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.ListeningWhitelist;
import com.comphenix.protocol.events.PacketEvent;
import com.comphenix.protocol.events.PacketListener;
import com.comphenix.protocol.injector.GamePhase;
import com.dracade.vone.bukkit.VoneBukkit;
import org.bukkit.entity.Player;

import java.util.Locale;

public class PlayerLocaleResolver implements PacketListener, LocaleResolver {

    @Override
    public void onPacketSending(PacketEvent event) {
        return;
    }

    @Override
    public void onPacketReceiving(PacketEvent event) {
        if (!(event.getPacket().getType() == PacketType.Play.Client.SETTINGS)) return;

        if (event.getPlayer() != null) {
            String language = event.getPacket().getStrings().readSafely(0);
            this.trySetPlayerLocale(event.getPlayer(), new Locale(language.substring(0, 2).toLowerCase()));
        }
    }

    @Override
    public ListeningWhitelist getSendingWhitelist() {
        return ListeningWhitelist.newBuilder().gamePhase(GamePhase.PLAYING).highest().types(PacketType.Play.Client.SETTINGS).build();
    }

    @Override
    public ListeningWhitelist getReceivingWhitelist() {
        return ListeningWhitelist.newBuilder().gamePhase(GamePhase.PLAYING).highest().types(PacketType.Play.Client.SETTINGS).build();
    }

    @Override
    public VoneBukkit getPlugin() {
        return VoneBukkit.plugin();
    }

    @Override
    public Locale resolveLocale(Player player) {
        return VoneBukkit.plugin().getPlayerLocale(player);
    }

    @Override
    public boolean trySetPlayerLocale(Player player, Locale locale) {
        VoneBukkit.plugin().setPlayerLocale(player, locale);
        return true;
    }

}

