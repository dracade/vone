package com.dracade.vone.bukkit.plugin.executors;

import com.dracade.vone.bukkit.VoneBukkit;
import com.dracade.vone.bukkit.api.game.Arena;
import com.dracade.vone.core.api.Request;
import com.dracade.vone.bukkit.plugin.VoneCommand;
import com.dracade.vone.bukkit.api.system.VoneEditor;
import com.dracade.vone.bukkit.api.system.VoneExecutor;
import com.dracade.vone.bukkit.api.utilities.FancyMessage;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.util.*;

public class SaveExecutor implements VoneExecutor {

    @Override
    public boolean execute(CommandSender sender, String[] args) throws ArrayIndexOutOfBoundsException {
        final Locale locale;

        if (sender instanceof Player) {
            locale = VoneBukkit.plugin().getPlayerLocale((Player) sender);
        } else {
            locale = VoneBukkit.plugin().getDefaultLocale();
        }

        if (sender.hasPermission("vone.save")) {
            if (sender instanceof Player) {
                Player player = ((Player) sender);

                Optional<Arena> arena = VoneEditor.Handler.get(player.getUniqueId());
                if (arena.isPresent()) {
                    Optional<VoneEditor> editor = VoneCommand.instance().getEditor(arena.get().getClass());
                    if (editor.isPresent()) {
                        List<String> leftoverArguments = new ArrayList<String>(Arrays.asList(args));
                        if (args.length > 0) {
                            leftoverArguments.remove(0);
                        }

                        Optional<Arena> toSave = editor.get().onSaveExecutor(player, arena.get(), leftoverArguments.toArray(new String[leftoverArguments.size()]));

                        if (toSave.isPresent()) {
                            try {
                                VoneBukkit.plugin().registry().save(arena.get());
                                player.sendMessage(VoneBukkit.plugin().i18n().translateDirect(locale, "messages.informative.arena-saved"));
                            } catch (FileAlreadyExistsException e) {
                                Request.Handler.create(new Request() {

                                    @Override
                                    public void onCreate(UUID id) {
                                        FancyMessage.PlaceholderMessage message = new FancyMessage.PlaceholderMessage(VoneBukkit.plugin().i18n().translateDirect(locale, "messages.informative.save-request"));

                                        /**
                                         * Add an {{ACCEPT}} placeholder.
                                         */
                                        message.placeholder("{{ACCEPT}}", (placeholder, color) -> {
                                            placeholder.then("[SAVE]")
                                                    .tooltip(VoneBukkit.plugin().i18n().translateDirect(locale, "messages.informative.save-request-accept-tooltip"))
                                                    .command("/" + VoneBukkit.plugin().getDescription().getName().toLowerCase() + " " + "request accept" + " " + id.toString())
                                                    .color(color);
                                        });

                                        /**
                                         * Add an {{DECLINE}} placeholder.
                                         */
                                        message.placeholder("{{DECLINE}}", ((placeholder, color) -> {
                                            placeholder.then("[CANCEL]")
                                                    .tooltip(VoneBukkit.plugin().i18n().translateDirect(locale, "messages.informative.save-request-decline-tooltip"))
                                                    .command("/" + VoneBukkit.plugin().getDescription().getName().toLowerCase() + " " + "request decline" + " " + id.toString())
                                                    .color(color);
                                        }));

                                        message.build().send(player);
                                    }

                                    @Override
                                    public void onAccept() {
                                        try {
                                            VoneBukkit.plugin().registry().save(arena.get(), true);
                                            player.sendMessage(VoneBukkit.plugin().i18n().translateDirect(locale, "messages.informative.arena-saved"));
                                        } catch (IOException ignored) {
                                        }
                                    }

                                    @Override
                                    public void onDecline() {
                                        player.sendMessage(VoneBukkit.plugin().i18n().translateDirect(locale, "messages.informative.arena-not-saved"));
                                    }

                                });
                            }
                        } else {
                            player.sendMessage(VoneBukkit.plugin().i18n().translateDirect(locale, "messages.errors.cannot-save-arena"));
                        }
                    } else {
                        player.sendMessage(VoneBukkit.plugin().i18n().translateDirect(locale, "messages.errors.unsupported-arena-type"));
                    }
                } else {
                    player.sendMessage(VoneBukkit.plugin().i18n().translateDirect(locale, "messages.informative.editor-mode-required"));
                }
            } else {
                sender.sendMessage(VoneBukkit.plugin().i18n().translateDirect(locale, "messages.errors.player-only-command"));
            }
        } else {
            sender.sendMessage(VoneBukkit.plugin().i18n().translateDirect(locale, "messages.errors.no-permission"));
        }

        return true;
    }

}
