package com.dracade.vone.bukkit.plugin;

import com.dracade.vone.bukkit.VoneBukkit;
import com.dracade.vone.bukkit.api.game.Arena;
import com.dracade.vone.bukkit.api.system.VoneEditor;
import com.dracade.vone.bukkit.api.system.VoneExecutor;
import com.dracade.vone.bukkit.plugin.executors.*;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.*;

public final class VoneCommand implements CommandExecutor, VoneExecutor {

    private static VoneCommand instance;

    /**
     * Retrieve the VoneCommand instance.
     *
     * @return VoneCommand
     */
    public static VoneCommand instance() {
        return (VoneCommand.instance != null) ? VoneCommand.instance : (VoneCommand.instance = new VoneCommand());
    }

    private Set<VoneEditor> editors;

    private VoneExecutor createExecutor;
    private VoneExecutor deleteExecutor;
    private VoneExecutor editExecutor;
    private VoneExecutor infoExecutor;
    private VoneExecutor joinExecutor;
    private VoneExecutor listExecutor;
    private VoneExecutor quitExecutor;
    private VoneExecutor exitExecutor;
    private VoneExecutor requestExecutor;
    private VoneExecutor saveExecutor;
    private VoneExecutor selectExecutor;
    private VoneExecutor teleportExecutor;

    /*
     * Create a new VoneCommand instance.
     */
    private VoneCommand() {
        this.editors = new HashSet<VoneEditor>();

        this.createExecutor = new CreateExecutor();
        this.deleteExecutor = new DeleteExecutor();
        this.editExecutor = new EditExecutor();
        this.infoExecutor = new InfoExecutor();
        this.joinExecutor = new JoinExecutor();
        this.listExecutor = new ListExecutor();
        this.quitExecutor = new QuitExecutor();
        this.exitExecutor = new ExitExecutor();
        this.requestExecutor = new RequestExecutor();
        this.saveExecutor = new SaveExecutor();
        this.selectExecutor = new SelectExecutor();
        this.teleportExecutor = new TeleportExecutor();
    }

    /**
     * Register an editor.
     *
     * @param editor
     */
    public VoneCommand register(Class<? extends VoneEditor> editor) {
        Preconditions.checkNotNull(editor);

        if (!this.editors.stream().anyMatch(e -> e.getClass().equals(editor))) {
            try {
                this.editors.add(editor.newInstance());
            } catch (InstantiationException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return this;
    }

    /**
     * Unregister a creator for the specified arena type.
     *
     * @param type
     */
    public VoneCommand unregister(Class<? extends VoneEditor> type) {
        Optional<VoneEditor> editor = this.editors.stream().filter(e -> e.getClass().equals(type)).findFirst();
        if (editor.isPresent()) {
            this.editors.remove(editor);
        }
        return this;
    }

    /**
     * Get all of the arena editors.
     *
     * @return
     */
    public ImmutableList<VoneEditor> getEditors() {
        return ImmutableList.copyOf(this.editors);
    }

    /**
     * Retrieve a list of commands.
     *
     * @return
     */
    public <T extends Arena> Optional<VoneEditor> getEditor(Class<T> type) {
        return this.getEditor(type.getSimpleName());
    }

    /**
     * Retrieve a list of commands.
     *
     * @return
     */
    public Optional<VoneEditor> getEditor(String type) {
        return this.editors.stream().filter(e -> e.getArenaType().getSimpleName().equalsIgnoreCase(type)).findFirst();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        final Locale locale;

        if (sender instanceof Player) {
            locale = VoneBukkit.plugin().getPlayerLocale((Player) sender);
        } else {
            locale = VoneBukkit.plugin().getDefaultLocale();
        }

        if (label.equalsIgnoreCase(VoneBukkit.plugin().getDescription().getName())) {
            if (!this.execute(sender, args)) {
                sender.sendMessage(VoneBukkit.plugin().i18n().translateDirect(locale, "messages.errors.incorrect-command-usage"));
            }
            return true;
        }
        return false;
    }

    @Override
    public boolean execute(CommandSender sender, String[] args) {
        final Locale locale;

        if (sender instanceof Player) {
            locale = VoneBukkit.plugin().getPlayerLocale((Player) sender);
        } else {
            locale = VoneBukkit.plugin().getDefaultLocale();
        }

        try {
            if (args.length > 0) {
                List<String> argsRemove = new ArrayList<String>(Arrays.asList(args));
                if (args.length > 0) {
                    argsRemove.remove(0);
                }

                String[] leftoverArguments = argsRemove.toArray(new String[argsRemove.size()]);

                if (args[0].equalsIgnoreCase("create")) {
                    return this.createExecutor.execute(sender, leftoverArguments);
                } else if (args[0].equalsIgnoreCase("delete")) {
                    return this.deleteExecutor.execute(sender, leftoverArguments);
                } else if (args[0].equalsIgnoreCase("edit")) {
                    return this.editExecutor.execute(sender, leftoverArguments);
                } else if (args[0].equalsIgnoreCase("exit")) {
                    return this.exitExecutor.execute(sender, leftoverArguments);
                } else if (args[0].equalsIgnoreCase("info")) {
                    return this.infoExecutor.execute(sender, leftoverArguments);
                } else if (args[0].equalsIgnoreCase("join")) {
                    return this.joinExecutor.execute(sender, leftoverArguments);
                } else if (args[0].equalsIgnoreCase("list")) {
                    return this.listExecutor.execute(sender, leftoverArguments);
                } else if (args[0].equalsIgnoreCase("quit")) {
                    return this.quitExecutor.execute(sender, leftoverArguments);
                } else if (args[0].equalsIgnoreCase("request")) {
                    return this.requestExecutor.execute(sender, leftoverArguments);
                } else if (args[0].equalsIgnoreCase("save")) {
                    return this.saveExecutor.execute(sender, leftoverArguments);
                } else if (args[0].equalsIgnoreCase("select")) {
                    return this.selectExecutor.execute(sender, leftoverArguments);
                } else if (args[0].equalsIgnoreCase("teleport")) {
                    return this.teleportExecutor.execute(sender, leftoverArguments);
                }

            } else {
                return VoneBukkit.plugin().sendPluginInformation(VoneBukkit.plugin(), VoneBukkit.plugin().i18n(), sender);
            }
        } catch (ArrayIndexOutOfBoundsException ignored) {
            // TODO: Invalid Arguments message ...
        }

        return false;
    }

}
