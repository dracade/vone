package com.dracade.vone.bukkit.plugin.executors;

import com.dracade.vone.bukkit.VoneBukkit;
import com.dracade.vone.bukkit.api.game.Arena;
import com.dracade.vone.bukkit.api.system.VoneExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Locale;
import java.util.Optional;

public class DeleteExecutor implements VoneExecutor {

    @Override
    public boolean execute(CommandSender sender, String[] args) throws ArrayIndexOutOfBoundsException {
        final Locale locale;

        if (sender instanceof Player) {
            locale = VoneBukkit.plugin().getPlayerLocale((Player) sender);
        } else {
            locale = VoneBukkit.plugin().getDefaultLocale();
        }

        if (sender.hasPermission("vone.delete")) {
            Optional<Arena> arena = VoneBukkit.plugin().registry().getArenas().stream().filter(a -> a.getName().equalsIgnoreCase(args[0])).findFirst();
            if (arena.isPresent()) {
                VoneBukkit.plugin().registry().delete(arena.get());
                sender.sendMessage(VoneBukkit.plugin().i18n().translateDirect(locale, "messages.informative.arena-deleted").replace("{{ARENA_NAME}}", args[0]));
            } else {
                sender.sendMessage(VoneBukkit.plugin().i18n().translateDirect(locale, "messages.errors.arena-not-found").replace("{{ARENA_NAME}}", args[0]));
            }
        } else {
            sender.sendMessage(VoneBukkit.plugin().i18n().translateDirect(locale, "messages.errors.no-permission"));
        }
        return true;
    }
}
