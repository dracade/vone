package com.dracade.vone.bukkit.plugin.executors;

import com.dracade.vone.bukkit.VoneBukkit;
import com.dracade.vone.bukkit.plugin.VoneCommand;
import com.dracade.vone.bukkit.api.system.VoneEditor;
import com.dracade.vone.bukkit.api.system.VoneExecutor;
import com.dracade.vone.bukkit.api.game.Arena;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.*;

public class CreateExecutor implements VoneExecutor {

    @Override
    public boolean execute(CommandSender sender, String[] args) throws ArrayIndexOutOfBoundsException {
        final Locale locale;

        if (sender instanceof Player) {
            locale = VoneBukkit.plugin().getPlayerLocale((Player) sender);
        } else {
            locale = VoneBukkit.plugin().getDefaultLocale();
        }

        if (sender.hasPermission("vone.create")) {
            if (sender instanceof Player) {
                Player player = ((Player) sender);
                if (VoneEditor.Handler.has(player.getUniqueId())) {
                    player.sendMessage(VoneBukkit.plugin().i18n().translateDirect(locale, "messages.errors.must-leave-editor"));
                } else {
                    if (args[0].equalsIgnoreCase("list")) {
                        if (VoneCommand.instance().getEditors().size() > 0) {
                            sender.sendMessage("");

                            VoneCommand.instance().getEditors().stream().forEach(editor -> {
                                sender.sendMessage("  " + VoneBukkit.plugin().i18n().translateDirect(locale, "settings.commands.create-list-format").replace("{{ARENA}}", editor.getArenaType().getSimpleName()));
                            });

                            sender.sendMessage("");
                        } else {
                            sender.sendMessage(VoneBukkit.plugin().i18n().translateDirect(locale, "messages.errors.no-supported-arenas"));
                        }
                    } else {
                        /**
                         * Determine the specified arena type
                         */
                        Optional<VoneEditor> editor = VoneCommand.instance().getEditor(args[0]);

                        /**
                         * If the specified arena type exists, then place the player into the editor
                         */
                        if (editor.isPresent()) {
                            if (args[1].equalsIgnoreCase("help")) {
                                sender.sendMessage("");
                                sender.sendMessage("  " + VoneBukkit.plugin().i18n().translateDirect(locale, "settings.commands.header").replace("{{SUB_COMMAND}}", "create"));
                                sender.sendMessage("");

                                editor.get().getCreateExecutorUsage().stream().forEach(usage -> {
                                    String message = VoneBukkit.plugin().i18n().translateDirect(locale, "settings.commands.format")
                                            .replace("{{COMMAND}}", VoneBukkit.plugin().getDescription().getName().toLowerCase())
                                            .replace("{{SUB_COMMAND}}", "create")
                                            .replace("{{USAGE}}", editor.get().getArenaType().getSimpleName() + " " + usage);

                                    player.sendMessage("  " + message);
                                });
                                player.sendMessage("");
                            } else {
                                List<String> leftoverArguments = new ArrayList<String>(Arrays.asList(args));
                                if (args.length > 0) {
                                    leftoverArguments.remove(0);
                                }

                                Optional<Arena> arena = editor.get().onCreateExecutor(player, leftoverArguments.toArray(new String[leftoverArguments.size()]));

                                if (arena.isPresent()) {
                                    VoneEditor.Handler.create(player.getUniqueId(), arena.get());
                                    String message = VoneBukkit.plugin().i18n().translateDirect(locale, "messages.informative.editor-mode-enabled")
                                            .replace("{{ARENA_NAME}}", arena.get().getName());

                                    player.sendMessage(message);
                                } else {
                                    return false;
                                }
                            }
                        } else {
                            player.sendMessage(VoneBukkit.plugin().i18n().translateDirect(locale, "messages.errors.unsupported-arena-type"));
                        }
                    }
                }
            } else {
                sender.sendMessage(VoneBukkit.plugin().i18n().translateDirect(locale, "messages.errors.player-only-command"));
            }
        } else {
            sender.sendMessage(VoneBukkit.plugin().i18n().translateDirect(locale, "messages.errors.no-permission"));
        }

        return true;
    }

}
