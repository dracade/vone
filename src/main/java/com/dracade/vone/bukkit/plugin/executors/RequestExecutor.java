package com.dracade.vone.bukkit.plugin.executors;

import com.dracade.vone.bukkit.VoneBukkit;
import com.dracade.vone.core.api.Request;
import com.dracade.vone.bukkit.api.system.VoneExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Locale;
import java.util.UUID;

public class RequestExecutor implements VoneExecutor {

    @Override
    public boolean execute(CommandSender sender, String[] args) throws ArrayIndexOutOfBoundsException {
        final Locale locale;

        if (sender instanceof Player) {
            locale = VoneBukkit.plugin().getPlayerLocale((Player) sender);
        } else {
            locale = VoneBukkit.plugin().getDefaultLocale();
        }

        if (sender.hasPermission("vone.request")) {
            if (sender instanceof Player) {
                if (args[0].equalsIgnoreCase("accept")) {
                    try {
                        UUID id = UUID.fromString(args[1]);
                        if (Request.Handler.has(id)) {
                            Request.Handler.accept(id);
                        } else {
                            sender.sendMessage(VoneBukkit.plugin().i18n().translateDirect(locale, "messages.errors.invalid-request-id"));
                        }
                    } catch (IllegalArgumentException e) {
                        sender.sendMessage(VoneBukkit.plugin().i18n().translateDirect(locale, "messages.errors.invalid-request-id"));
                    }
                } else if (args[0].equalsIgnoreCase("decline")) {
                    try {
                        UUID id = UUID.fromString(args[1]);
                        if (Request.Handler.has(id)) {
                            Request.Handler.decline(id);
                        } else {
                            sender.sendMessage(VoneBukkit.plugin().i18n().translateDirect(locale, "messages.errors.invalid-request-id"));
                        }
                    } catch (IllegalArgumentException e) {
                        sender.sendMessage(VoneBukkit.plugin().i18n().translateDirect(locale, "messages.errors.invalid-request-id"));
                    }
                } else {
                    return false;
                }
            } else {
                sender.sendMessage(VoneBukkit.plugin().i18n().translateDirect(locale, "messages.errors.player-only-command"));
            }
        } else {
            sender.sendMessage(VoneBukkit.plugin().i18n().translateDirect(locale, "messages.errors.no-permission"));
        }

        return true;
    }

}
