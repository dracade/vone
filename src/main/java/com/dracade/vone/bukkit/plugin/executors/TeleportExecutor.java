package com.dracade.vone.bukkit.plugin.executors;

import com.dracade.vone.bukkit.VoneBukkit;
import com.dracade.vone.bukkit.api.game.Arena;
import com.dracade.vone.bukkit.api.system.VoneExecutor;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Locale;
import java.util.Optional;

public class TeleportExecutor implements VoneExecutor {

    @Override
    public boolean execute(CommandSender sender, String[] args) throws ArrayIndexOutOfBoundsException {
        final Locale locale;

        if (sender instanceof Player) {
            locale = VoneBukkit.plugin().getPlayerLocale((Player) sender);
        } else {
            locale = VoneBukkit.plugin().getDefaultLocale();
        }

        if (sender.hasPermission("vone.teleport")) {
            if (args.length > 0) {
                if (sender instanceof Player) {
                    Player player = (Player) sender;

                    /**
                     * Teleport to arena
                     */
                    if (args[0].equalsIgnoreCase("arena")) {
                        Optional<Arena> arena = VoneBukkit.plugin().registry().getArenas().stream().filter(a -> a.getName().equalsIgnoreCase(args[1])).findFirst();
                        if (arena.isPresent()) {
                            sender.sendMessage(VoneBukkit.plugin().i18n().translateDirect(locale, "messages.informative.teleport-to-arena").replace("{{ARENA_NAME}}", arena.get().getName()));
                            player.teleport(arena.get().getSpawnLocation());
                        } else {
                            sender.sendMessage(VoneBukkit.plugin().i18n().translateDirect(locale, "messages.errors.arena-not-found").replace("{{ARENA_NAME}}", args[1]));
                        }

                    }

                    /**
                     * Teleport to world
                     */
                    else if (args[0].equalsIgnoreCase("world")) {
                        if (Bukkit.getWorld(args[1]) != null) {
                            World world = Bukkit.getWorld(args[1]);
                            sender.sendMessage(VoneBukkit.plugin().i18n().translateDirect(locale, "messages.informative.teleport-to-world").replace("{{WORLD_NAME}}", world.getName()));
                            player.teleport(world.getSpawnLocation());
                        } else {
                            sender.sendMessage(VoneBukkit.plugin().i18n().translateDirect(locale, "messages.errors.world-not-found").replace("{{WORLD_NAME}}", args[1]));
                        }
                    }

                    /**
                     * Incorrect usage
                     */
                    else {
                        return false;
                    }
                } else {
                    sender.sendMessage(VoneBukkit.plugin().i18n().translateDirect(locale, "messages.errors.player-only-command"));
                }

            /**
             * Incorrect usage
             */
            } else {
                return false;
            }
        } else {
            sender.sendMessage(VoneBukkit.plugin().i18n().translateDirect(locale, "messages.errors.no-permission"));
        }
        return true;
    }

}
