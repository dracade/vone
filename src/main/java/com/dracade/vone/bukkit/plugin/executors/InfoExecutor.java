package com.dracade.vone.bukkit.plugin.executors;

import com.dracade.vone.bukkit.VoneBukkit;
import com.dracade.vone.bukkit.api.game.Arena;
import com.dracade.vone.bukkit.plugin.VoneCommand;
import com.dracade.vone.bukkit.api.system.VoneEditor;
import com.dracade.vone.bukkit.api.system.VoneExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.*;

public class InfoExecutor implements VoneExecutor {

    @Override
    public boolean execute(CommandSender sender, String[] args) throws ArrayIndexOutOfBoundsException {
        final Locale locale;

        if (sender instanceof Player) {
            locale = VoneBukkit.plugin().getPlayerLocale((Player) sender);
        } else {
            locale = VoneBukkit.plugin().getDefaultLocale();
        }

        if (sender.hasPermission("vone.info")) {

            if (sender instanceof Player) {
                Player player = ((Player) sender);
                Optional<Arena> arena = VoneEditor.Handler.get(player.getUniqueId());
                if (arena.isPresent()) {
                    Optional<VoneEditor> editor = VoneCommand.instance().getEditor(arena.get().getClass());
                    if (editor.isPresent()) {
                        if (args.length > 0) {
                            if (args[0].equalsIgnoreCase("help")) {
                                sender.sendMessage("");
                                sender.sendMessage("  " + VoneBukkit.plugin().i18n().translateDirect(locale, "settings.commands.header").replace("{{SUB_COMMAND}}", "create"));
                                sender.sendMessage("");

                                editor.get().getInfoExecutorUsage().stream().forEach(usage -> {
                                    String message = VoneBukkit.plugin().i18n().translateDirect(locale, "settings.commands.format")
                                            .replace("{{COMMAND}}", VoneBukkit.plugin().getDescription().getName().toLowerCase())
                                            .replace("{{SUB_COMMAND}}", "info")
                                            .replace("{{USAGE}}", usage);

                                    player.sendMessage("  " + message);
                                });
                                sender.sendMessage("");

                                return true;
                            }
                        }

                        return editor.get().onInfoExecutor(player, arena.get(), args);

                    } else {
                        player.sendMessage(VoneBukkit.plugin().i18n().translateDirect(locale, "messages.errors.unsupported-arena-type"));
                    }
                } else {
                    player.sendMessage(VoneBukkit.plugin().i18n().translateDirect(locale, "messages.informative.editor-mode-required"));
                }
            } else {
                sender.sendMessage(VoneBukkit.plugin().i18n().translateDirect(locale, "messages.errors.player-only-command"));
            }
        } else {
            sender.sendMessage(VoneBukkit.plugin().i18n().translateDirect(locale, "messages.errors.no-permission"));
        }

        return true;
    }

}
