package com.dracade.vone.bukkit.plugin.executors;

import com.dracade.vone.bukkit.api.game.Arena;
import com.dracade.vone.bukkit.api.game.Minigame;
import com.dracade.vone.bukkit.VoneBukkit;
import com.dracade.vone.bukkit.api.system.VoneExecutor;
import com.dracade.vone.bukkit.api.utilities.FancyMessage;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Locale;

public class ListExecutor implements VoneExecutor {

    @Override
    public boolean execute(CommandSender sender, String[] args) throws ArrayIndexOutOfBoundsException {
        final Locale locale;

        if (sender instanceof Player) {
            locale = VoneBukkit.plugin().getPlayerLocale((Player) sender);
        } else {
            locale = VoneBukkit.plugin().getDefaultLocale();
        }

        if (sender.hasPermission("vone.list")) {

            /**
             * List arenas
             */
            if (args[0].equalsIgnoreCase("arenas")) {
                if (VoneBukkit.plugin().registry().getArenas().size() > 0) {

                    sender.sendMessage("");
                    for (Arena a : VoneBukkit.plugin().registry().getArenas()) {
                        FancyMessage.PlaceholderMessage message = new FancyMessage.PlaceholderMessage(VoneBukkit.plugin().i18n().translateDirect(locale, "settings.commands.list-arena-format"));

                        /**
                         * Add an {{ARENA_NAME}} placeholder.
                         */
                        message.placeholder("{{ARENA_NAME}}", (placeholder, color) -> {
                            placeholder.then(a.getName())
                                    .tooltip(VoneBukkit.plugin().i18n().translateDirect(locale, "settings.commands.list-arena-tooltip"))
                                    .suggest("/" + VoneBukkit.plugin().getDescription().getName().toLowerCase() + " " + "select" + " " + a.getName())
                                    .color(color);
                        });

                        /**
                         * Add an {{ARENA_DISPLAY_NAME}} placeholder.
                         */
                        message.placeholder("{{ARENA_DISPLAY_NAME}}", (placeholder, color) -> {
                            placeholder.then(a.getDisplayName())
                                    .tooltip(VoneBukkit.plugin().i18n().translateDirect(locale, "settings.commands.list-arena-tooltip"))
                                    .suggest("/" + VoneBukkit.plugin().getDescription().getName().toLowerCase() + " " + "select" + " " + a.getName())
                                    .color(color);
                        });

                        message.build().then("").send(sender);
                    }
                    sender.sendMessage("");
                } else {
                    sender.sendMessage(VoneBukkit.plugin().i18n().translateDirect(locale, "messages.informative.no-arenas"));
                }
            }

            /**
             * List minigames
             */
            else if (args[0].equalsIgnoreCase("minigames")) {
                if (VoneBukkit.plugin().registry().getActiveArenas().size() > 0) {

                    sender.sendMessage("");
                    for (Minigame m : VoneBukkit.plugin().registry().getMinigames()) {
                        FancyMessage.PlaceholderMessage message = new FancyMessage.PlaceholderMessage(VoneBukkit.plugin().i18n().translateDirect(locale, "settings.commands.list-minigame-format"));

                        /**
                         * Add a {{MINIGAME}} placeholder.
                         */
                        message.placeholder("{{MINIGAME}}", (placeholder, color) -> {
                            placeholder.then(m.getClass().getSimpleName())
                                    .tooltip(VoneBukkit.plugin().i18n().translateDirect(locale, "settings.commands.list-minigame-tooltip"))
                                    .suggest("/" + VoneBukkit.plugin().getDescription().getName().toLowerCase() + " " + "join" + " " + m.getUniqueId())
                                    .color(color);
                        });

                        /**
                         * Add a {{ARENA_DISPLAY_NAME}} placeholder.
                         */
                        message.placeholder("{{ARENA_DISPLAY_NAME}}", ((placeholder, color) -> {
                            placeholder.then(m.getArena().getDisplayName())
                                    .color(color);
                        }));

                        message.build().send(sender);
                    }
                    sender.sendMessage("");
                } else {
                    sender.sendMessage(VoneBukkit.plugin().i18n().translateDirect(locale, "messages.informative.no-minigames"));
                }
            }

            /**
             * Incorrect usage
             */
            else {
                return false;
            }
        } else {
            sender.sendMessage(VoneBukkit.plugin().i18n().translateDirect(locale, "messages.errors.no-permission"));
        }

        return true;
    }

}
