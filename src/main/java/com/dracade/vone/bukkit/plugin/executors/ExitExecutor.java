package com.dracade.vone.bukkit.plugin.executors;

import com.dracade.vone.bukkit.VoneBukkit;
import com.dracade.vone.bukkit.api.game.Arena;
import com.dracade.vone.core.api.Request;
import com.dracade.vone.bukkit.api.system.VoneEditor;
import com.dracade.vone.bukkit.api.system.VoneExecutor;
import com.dracade.vone.bukkit.api.utilities.FancyMessage;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Locale;
import java.util.Optional;
import java.util.UUID;

public class ExitExecutor implements VoneExecutor {

    @Override
    public boolean execute(CommandSender sender, String[] args) throws ArrayIndexOutOfBoundsException {
        final Locale locale;

        if (sender instanceof Player) {
            locale = VoneBukkit.plugin().getPlayerLocale((Player) sender);
        } else {
            locale = VoneBukkit.plugin().getDefaultLocale();
        }

        if (sender.hasPermission("vone.exit")) {
            if (sender instanceof Player) {
                Player player = ((Player) sender);

                if (VoneEditor.Handler.has(player.getUniqueId())) {
                    Request.Handler.create(new Request() {

                        @Override
                        public void onCreate(UUID id) {
                            FancyMessage.PlaceholderMessage message = new FancyMessage.PlaceholderMessage(VoneBukkit.plugin().i18n().translateDirect(locale, "messages.informative.exit-request"));

                            /**
                             * Add an {{ACCEPT}} placeholder.
                             */
                            message.placeholder("{{ACCEPT}}", (placeholder, color) -> {
                                placeholder.then("[EXIT]")
                                        .tooltip(VoneBukkit.plugin().i18n().translateDirect(locale, "messages.informative.exit-request-accept-tooltip"))
                                        .command("/" + VoneBukkit.plugin().getDescription().getName().toLowerCase() + " " + "request accept" + " " + id.toString())
                                        .color(color);
                            });

                            /**
                             * Add an {{DECLINE}} placeholder.
                             */
                            message.placeholder("{{DECLINE}}", (placeholder, color) -> {
                                placeholder.then("[CANCEL]")
                                        .tooltip(VoneBukkit.plugin().i18n().translateDirect(locale, "messages.informative.exit-request-decline-tooltip"))
                                        .command("/" + VoneBukkit.plugin().getDescription().getName().toLowerCase() + " " + "request decline" + " " + id.toString())
                                        .color(color);
                            });

                            message.build().send(player);
                        }

                        @Override
                        public void onAccept() {
                            VoneEditor.Handler.destroy(player.getUniqueId());
                            player.sendMessage(VoneBukkit.plugin().i18n().translateDirect(locale, "messages.informative.exit-editor"));
                        }

                        @Override
                        public void onDecline() {
                            Optional<Arena> arena = VoneEditor.Handler.get(player.getUniqueId());
                            if (arena.isPresent()) {
                                String message = VoneBukkit.plugin().i18n().translateDirect(locale, "messages.informative.editor-mode-enabled")
                                        .replace("{{ARENA_NAME}}", arena.get().getName());

                                sender.sendMessage(message);
                            } else {
                                sender.sendMessage(VoneBukkit.plugin().i18n().translateDirect(locale, "messages.errors.internal-error"));
                            }
                        }

                    });
                } else {
                    sender.sendMessage(VoneBukkit.plugin().i18n().translateDirect(locale, "messages.informative.editor-mode-required"));
                }
            } else {
                sender.sendMessage(VoneBukkit.plugin().i18n().translateDirect(locale, "messages.errors.player-only-command"));
            }
        } else {
            sender.sendMessage(VoneBukkit.plugin().i18n().translateDirect(locale, "messages.errors.no-permission"));
        }

        return true;
    }

}
