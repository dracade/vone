package com.dracade.vone.bukkit.plugin.executors;

import com.dracade.vone.bukkit.VoneBukkit;
import com.dracade.vone.bukkit.api.game.Playable;
import com.dracade.vone.bukkit.api.game.Minigame;
import com.dracade.vone.bukkit.api.system.VoneExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Locale;
import java.util.Optional;

public class QuitExecutor implements VoneExecutor {

    @Override
    public boolean execute(CommandSender sender, String[] args) throws ArrayIndexOutOfBoundsException {
        final Locale locale;

        if (sender instanceof Player) {
            locale = VoneBukkit.plugin().getPlayerLocale(((Player) sender));
        } else {
            locale = VoneBukkit.plugin().getDefaultLocale();
        }

        if (sender.hasPermission("vone.quit")) {
            if (sender instanceof Player) {
                Player player = ((Player) sender);
                Optional<Minigame> minigame = VoneBukkit.plugin().registry().getMinigame(player);

                if (minigame.isPresent()) {
                    if (minigame.get() instanceof Playable) {
                        ((Playable) minigame.get()).removePlayer(player.getUniqueId());
                        sender.sendMessage(VoneBukkit.plugin().i18n().translateDirect(locale, "messages.informative.quitting-game"));
                    } else {
                        sender.sendMessage(VoneBukkit.plugin().i18n().translateDirect(locale, "messages.errors.not-joinable"));
                    }
                } else {
                    sender.sendMessage(VoneBukkit.plugin().i18n().translateDirect(locale, "messages.errors.not-in-minigame"));
                }

            } else {
                sender.sendMessage(VoneBukkit.plugin().i18n().translateDirect(locale, "messages.errors.player-only-command"));
            }
        } else {
            sender.sendMessage(VoneBukkit.plugin().i18n().translateDirect(locale, "messages.errors.no-permission"));
        }

        return true;
    }

}
