package com.dracade.vone.bukkit.plugin.executors;

import com.dracade.vone.bukkit.VoneBukkit;
import com.dracade.vone.bukkit.api.game.Arena;
import com.dracade.vone.bukkit.api.system.VoneEditor;
import com.dracade.vone.bukkit.api.system.VoneExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Locale;
import java.util.Optional;

public class SelectExecutor implements VoneExecutor {

    @Override
    public boolean execute(CommandSender sender, String[] args) throws ArrayIndexOutOfBoundsException {
        final Locale locale;

        if (sender instanceof Player) {
            locale = VoneBukkit.plugin().getPlayerLocale((Player) sender);
        } else {
            locale = VoneBukkit.plugin().getDefaultLocale();
        }

        if (sender.hasPermission("vone.select")) {
            if (sender instanceof Player) {
                Player player = ((Player) sender);

                Optional<Arena> arena = VoneBukkit.plugin().registry().getArenas().stream().filter(a -> a.getName().equalsIgnoreCase(args[0])).findFirst();
                if (arena.isPresent()) {
                    if (VoneEditor.Handler.has(player.getUniqueId())) {
                        sender.sendMessage(VoneBukkit.plugin().i18n().translateDirect(locale, "messages.errors.must-leave-editor"));
                    } else {
                        VoneEditor.Handler.create(player.getUniqueId(), arena.get());
                        String message = VoneBukkit.plugin().i18n().translateDirect(locale, "messages.informative.editor-mode-enabled")
                                .replace("{{ARENA_NAME}}", arena.get().getName());

                        sender.sendMessage(message);
                    }
                } else {
                    sender.sendMessage(VoneBukkit.plugin().i18n().translateDirect(locale, "messages.errors.arena-not-found").replace("{{ARENA_NAME}}", args[0]));
                }
            } else {
                sender.sendMessage(VoneBukkit.plugin().i18n().translateDirect(locale, "messages.errors.player-only-command"));
            }
        } else {
            sender.sendMessage(VoneBukkit.plugin().i18n().translateDirect(locale, "messages.errors.no-permission"));
        }

        return true;
    }
}
