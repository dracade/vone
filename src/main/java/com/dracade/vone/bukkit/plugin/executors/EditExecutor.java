package com.dracade.vone.bukkit.plugin.executors;

import com.dracade.vone.bukkit.VoneBukkit;
import com.dracade.vone.bukkit.plugin.VoneCommand;
import com.dracade.vone.bukkit.api.system.VoneEditor;
import com.dracade.vone.bukkit.api.system.VoneExecutor;
import com.dracade.vone.bukkit.api.game.Arena;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Locale;
import java.util.Optional;

public class EditExecutor implements VoneExecutor {

    @Override
    public boolean execute(CommandSender sender, String[] args) throws ArrayIndexOutOfBoundsException {
        final Locale locale;

        if (sender instanceof Player) {
            locale = VoneBukkit.plugin().getPlayerLocale((Player) sender);
        } else {
            locale = VoneBukkit.plugin().getDefaultLocale();
        }

        if (sender.hasPermission("vone.edit")) {
            if (sender instanceof Player) {
                Player player = ((Player) sender);

                Optional<Arena> arena = VoneEditor.Handler.get(player.getUniqueId());
                if (arena.isPresent()) {
                    Optional<VoneEditor> editor = VoneCommand.instance().getEditor(arena.get().getClass());
                    if (editor.isPresent()) {
                        if (args[0].equalsIgnoreCase("help")) {
                            sender.sendMessage("");
                            sender.sendMessage("  " + VoneBukkit.plugin().i18n().translateDirect(locale, "settings.commands.header").replace("{{SUB_COMMAND}}", "edit"));
                            sender.sendMessage("");

                            editor.get().getEditExecutorUsage().stream().forEach(usage -> {
                                String message = VoneBukkit.plugin().i18n().translateDirect(locale, "settings.commands.format")
                                        .replace("{{COMMAND}}", VoneBukkit.plugin().getDescription().getName().toLowerCase())
                                        .replace("{{SUB_COMMAND}}", "edit")
                                        .replace("{{USAGE}}", usage);

                                sender.sendMessage("  " + message);
                            });
                            sender.sendMessage("");
                        } else {
                            return editor.get().onEditExecutor(player, arena.get(), args);
                        }
                    } else {
                        player.sendMessage(VoneBukkit.plugin().i18n().translateDirect(locale, "messages.errors.unsupported-arena-type"));
                    }
                } else {
                    player.sendMessage(VoneBukkit.plugin().i18n().translateDirect(locale, "messages.informative.editor-mode-required"));
                }
            } else {
                sender.sendMessage(VoneBukkit.plugin().i18n().translateDirect(locale, "messages.errors.player-only-command"));
            }
        } else {
            sender.sendMessage(VoneBukkit.plugin().i18n().translateDirect(locale, "messages.errors.no-permission"));
        }

        return true;
    }

}
