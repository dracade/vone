package com.dracade.vone.bukkit.api.system;

import com.dracade.vone.bukkit.api.utilities.PermissionWeightComparator;
import com.google.common.collect.ImmutableList;

import java.util.List;
import java.util.UUID;

public interface VoneQueueItem extends Comparable<VoneQueueItem> {

    String permissionEvaluator = "vone.queue.priority.";

    /**
     * Retrieve the queue holder.
     *
     * @return
     */
    UUID getQueueHolder();

    /**
     * Return a list of player's that is contained
     * within this queue holder.
     *
     * @return
     */
    ImmutableList<UUID> getPlayers();

    /**
     * Retrieve the queue permission evaluator.
     *
     * @return
     */
    default String getQueuePermissionEvaluator() {
        return this.permissionEvaluator;
    }

    /**
     * Retrieve the weight of the queue item to determine it's priority.
     * The heavier the weight, the higher the priority.
     *
     * @return
     */
    default int getWeight() {
        return PermissionWeightComparator.getWeight(this.getQueuePermissionEvaluator(), this.getQueueHolder());
    }

    @Override
    default int compareTo(VoneQueueItem b) {
        int weightA = this.getWeight();
        int weightB = b.getWeight();

        if (weightA > weightB) {
            return 1;
        }
        else if (weightB > weightA) {
            return -1;
        } else {
            return 0;
        }
    }
}
