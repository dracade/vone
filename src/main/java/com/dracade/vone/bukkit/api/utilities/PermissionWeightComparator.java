package com.dracade.vone.bukkit.api.utilities;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionAttachmentInfo;

import java.security.InvalidParameterException;
import java.util.Comparator;
import java.util.Set;
import java.util.UUID;

public class PermissionWeightComparator implements Comparator<UUID> {

    private static final String invalidPermissionCheckMessage = "The specified permission must end with a \".\" in order for the weight to be calculated from the end value.";

    /**
     * Retrieve's the player's weight based on their permissions.
     *
     * @param permission The permission to determine the weight
     * @param paiSet The set to evaluate
     * @return The player's weight, note: it will return 0 if the specified permission is invalid
     * @throws NumberFormatException thrown if the permission checked against does not contain an integer
     */
    public static int getWeight(String permission, Set<PermissionAttachmentInfo> paiSet) throws NumberFormatException {
        int weight = 0;
        if (permission.endsWith(".")) {
            for (PermissionAttachmentInfo pai : paiSet) {
                String paiPerm = pai.getPermission().toLowerCase();
                if (paiPerm.startsWith(permission.toLowerCase())) {
                    String val = paiPerm.replace(permission.toLowerCase(), "");

                    // parse integer.
                    int i = Integer.parseInt(val);
                    if (i > weight) {
                        weight = i;
                    }
                }
            }
        }
        return weight;
    }

    /**
     * Get the weight of a specific player.
     *
     * @param permission The permission to determine the weight
     * @param uniqueId The player's unique id
     * @return The player's weight, note: it will return 0 if the specified permission is invalid or the player could not be found
     */
    public static int getWeight(String permission, UUID uniqueId) {
        Player player = Bukkit.getPlayer(uniqueId);
        if (player != null) {
            return PermissionWeightComparator.getWeight(permission, player.getEffectivePermissions());
        }
        return 0;
    }

    private String permission;

    /**
     * Determine someone's weight based on their permissions.
     *
     * @param permission The permission to determine the weight
     */
    public PermissionWeightComparator(String permission) throws InstantiationException {
        this.permission = permission;
        if (!permission.endsWith(".")) {
            throw new InstantiationException(PermissionWeightComparator.invalidPermissionCheckMessage);
        }
    }

    @Override
    public int compare(UUID a, UUID b) {
        Player playerA = Bukkit.getPlayer(a);
        Player playerB = Bukkit.getPlayer(b);

        int priority = 0;

        if (playerA != null && playerB != null) {
            Set<PermissionAttachmentInfo> paiASet = playerA.getEffectivePermissions();
            Set<PermissionAttachmentInfo> paiBSet = playerB.getEffectivePermissions();

            int weightA = 0;
            int weightB = 0;
            try {
                weightA = PermissionWeightComparator.getWeight(this.permission, paiASet);
                weightB = PermissionWeightComparator.getWeight(this.permission, paiBSet);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }

            if (weightA > weightB) {
                priority = 1;
            }
            else if (weightB > weightA) {
                priority = -1;
            }
        } else {
            if (playerA == null) {
                priority = -1;
            }
            else if (playerB == null) {
                priority = 1;
            }
        }

        return priority;
    }

}
