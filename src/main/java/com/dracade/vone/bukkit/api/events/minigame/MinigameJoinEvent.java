package com.dracade.vone.bukkit.api.events.minigame;

import com.dracade.vone.bukkit.api.game.Minigame;
import com.dracade.vone.bukkit.api.events.MinigameEvent;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;

public class MinigameJoinEvent extends MinigameEvent implements Cancellable {

    private static final HandlerList handlers = new HandlerList();

    /**
     * Retrieve the handlers associated with this event.
     *
     * @return A list of event handlers
     */
    public static HandlerList getHandlerList() {
        return MinigameJoinEvent.handlers;
    }

    private boolean cancelled;
    private Player player;

    /**
     * Create a new MinigameJoinEvent instance.
     *
     * @param minigame The minigame that is associated with the event
     */
    public MinigameJoinEvent(Minigame minigame, Player player) {
        super(minigame);
        this.cancelled = false;
        this.player = player;
    }

    @Override
    public HandlerList getHandlers() {
        return MinigameJoinEvent.handlers;
    }

    @Override
    public boolean isCancelled() {
        return this.cancelled;
    }

    @Override
    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    /**
     * Retrieve the player.
     *
     * @return The player instance
     */
    public Player getPlayer() {
        return this.player;
    }

}
