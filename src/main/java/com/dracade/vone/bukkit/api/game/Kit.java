package com.dracade.vone.bukkit.api.game;

import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.Optional;

public interface Kit {

    /**
     * Retrieve the inventory.
     *
     * @return
     */
    Optional<Inventory> getInventory();

    /**
     * Retrieve the armour contents.
     *
     * @return
     */
    default ItemStack[] getArmorContents() {
        return new ItemStack[] {
                this.getBoots().orElse(null),
                this.getLeggings().orElse(null),
                this.getChestplate().orElse(null),
                this.getHelmet().orElse(null)
        };
    }

    /**
     * Retrieve the helmet.
     *
     * @return
     */
    Optional<ItemStack> getHelmet();

    /**
     * Retrieve the chestplate.
     *
     * @return
     */
    Optional<ItemStack> getChestplate();

    /**
     * Retrieve the leggings.
     *
     * @return
     */
    Optional<ItemStack> getLeggings();

    /**
     * Retrieve the boots.
     * @return
     */
    Optional<ItemStack> getBoots();

    /**
     * Give a kit to the specified player.
     *
     * @param
     */
    default void give(Player player) {
        Optional<Inventory> inventory = this.getInventory();
        if (inventory.isPresent()) {
            player.getInventory().setContents(inventory.get().getContents());
        }
        player.getInventory().setArmorContents(this.getArmorContents());
        player.updateInventory();
    }

}
