package com.dracade.vone.bukkit.api.system;

import com.dracade.vone.bukkit.api.events.queue.QueueAddEvent;
import com.dracade.vone.bukkit.api.events.queue.QueueRemoveEvent;
import org.bukkit.Bukkit;

import java.util.*;

public abstract class VoneQueue <E extends VoneQueueItem> extends PriorityQueue<E> {

    @Override
    public boolean remove(Object o) {
        boolean result = super.remove(o);

        if (result) {
            Bukkit.getPluginManager().callEvent(new QueueRemoveEvent(this, (VoneQueueItem) o));
        }

        return result;
    }

    @Override
    public void clear() {
        for (VoneQueueItem i : this) {
            Bukkit.getPluginManager().callEvent(new QueueRemoveEvent(this, i));
        }
    }

    @Override
    public boolean offer(E o) {
        boolean result = super.offer(o);

        if (result) {
            Bukkit.getPluginManager().callEvent(new QueueAddEvent(this, o));
        }

        return result;
    }

    @Override
    public E poll() {
        E entity = super.poll();

        if (entity != null) {
            Bukkit.getPluginManager().callEvent(new QueueRemoveEvent(this, entity, true));
        }

        return entity;
    }

    /**
     * Check if the queue contains a specific player.
     *
     * @param player
     * @return
     */
    public boolean containsPlayer(UUID player) {
        return this.stream().anyMatch(a -> a.getPlayers().contains(player));
    }

}
