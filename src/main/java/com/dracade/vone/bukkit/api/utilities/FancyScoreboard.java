package com.dracade.vone.bukkit.api.utilities;

import java.util.AbstractMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import com.google.common.base.Preconditions;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * FancyScoreboard.
 *
 * This was updated and edited to follow the builder design pattern
 * in order to make life easier. The methods also have some very
 * basic commenting.
 *
 * @author RainoBoy97 (https://bukkit.org/members/rainoboy97.90728812/)
 */
public class FancyScoreboard {

    private Scoreboard scoreboard;

    private String title;
    private Map<String, Integer> scores;
    private List<Team> teams;

    /**
     * Create a FancyScoreboard.
     *
     * @param title The name of the board to display
     */
    public FancyScoreboard(String title) {
        this.scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
        this.title = title;
        this.scores = Maps.newLinkedHashMap();
        this.teams = Lists.newArrayList();
    }

    /**
     * Add a blank line to the board.
     * @return this
     */
    public FancyScoreboard blankLine() {
        add(" ");
        return this;
    }

    /**
     * Add a line of text to the board.
     *
     * @param text The text you wish to add
     * @return this
     */
    public FancyScoreboard add(String text) {
        add(text, null);
        return this;
    }

    /**
     * Add a line of text to the board with a given score.
     *
     * @param text The text you wish to add
     * @param score The score you wish to set
     * @return this
     */
    public FancyScoreboard add(String text, Integer score) {
        Preconditions.checkArgument(text.length() < 48, "text cannot be over 48 characters in length");
        text = fixDuplicates(text);
        this.scores.put(text, score);
        return this;
    }

    /**
     * Fix all duplicate lines of texts.
     *
     * @param text The line of text to fix
     * @return The resolved line of text
     */
    private String fixDuplicates(String text) {
        while (this.scores.containsKey(text)) {
            text += ChatColor.RESET;
        }
        if (text.length() > 48) {
            text = text.substring(0, 47);
        }
        return text;
    }

    /**
     * Create a team for the specified line of text.
     *
     * @param text The line of text
     * @return A map containing the team entry
     */
    private Map.Entry<Team, String> createTeam(String text) {
        String result = "";
        if (text.length() <= 16) {
            return new AbstractMap.SimpleEntry<>(null, text);
        }
        Team team = this.scoreboard.registerNewTeam("text-" + this.scoreboard.getTeams().size());
        Iterator<String> iterator = Splitter.fixedLength(16).split(text).iterator();
        team.setPrefix(iterator.next());
        result = iterator.next();
        if (text.length() > 32) {
            team.setSuffix(iterator.next());
        }
        this.teams.add(team);
        return new AbstractMap.SimpleEntry<>(team, result);
    }

    /**
     * Build the scoreboard.
     *
     * @return this
     */
    public FancyScoreboard build() {
        Objective obj = this.scoreboard.registerNewObjective((this.title.length() > 16 ? this.title.substring(0, 15) : this.title), "dummy");
        obj.setDisplayName(this.title);
        obj.setDisplaySlot(DisplaySlot.SIDEBAR);

        int index = scores.size();

        for (Map.Entry<String, Integer> text : scores.entrySet()) {
            Map.Entry<Team, String> team = createTeam(text.getKey());
            Integer score = text.getValue() != null ? text.getValue() : index;
            if (team.getKey() != null) {
                team.getKey().addEntry(team.getValue());
            }
            obj.getScore(team.getValue()).setScore(score);
            index -= 1;
        }
        return this;
    }

    /**
     * Reset the scoreboard.
     *
     * @return this
     */
    public FancyScoreboard reset() {
        return this.reset(false);
    }

    /**
     * Reset the scoreboard.
     *
     * @return this
     */
    public FancyScoreboard reset(boolean full) {
        if (full) {
            this.title = null;
        } else {
            this.scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
        }
        this.scores.clear();
        this.teams.stream().forEach(team -> {
            team.unregister();
        });
        this.teams.clear();
        return this;
    }

    /**
     * Retrieve the scoreboard.
     *
     * @return The scoreboard instance
     */
    public Scoreboard getScoreboard() {
        return this.scoreboard;
    }

    /**
     * Send the scoreboard.
     *
     * @param players The players to receive the scoreboard
     */
    public void send(Player... players) {
        for (Player p : players) {
            p.setScoreboard(this.scoreboard);
        }
    }

}