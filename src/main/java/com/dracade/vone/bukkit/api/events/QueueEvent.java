package com.dracade.vone.bukkit.api.events;

import com.dracade.vone.bukkit.api.game.Minigame;
import com.dracade.vone.bukkit.api.system.VoneQueue;
import com.dracade.vone.bukkit.api.system.VoneQueueItem;
import org.bukkit.event.Event;

public abstract class QueueEvent extends Event {

    private VoneQueue queue;
    private VoneQueueItem item;

    /**
     * Create a new QueueEvent instance.
     *
     * @param queue The queue that is associated with the event
     * @param item The queue iteme that is associated with the event
     */
    public QueueEvent(VoneQueue queue, VoneQueueItem item) {
        this.queue = queue;
        this.item = item;
    }

    /**
     * Retrieve the associated queue.
     *
     * @return VoneQueue instance
     */
    public VoneQueue getQueue() {
        return this.queue;
    }

    /**
     * Retrieve the associated queue item.
     *
     * @return VoneQueueItem instance
     */
    public VoneQueueItem getItem() {
        return this.item;
    }

}
