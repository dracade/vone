package com.dracade.vone.bukkit.api.game;

import com.dracade.vone.bukkit.api.serializer.Serializer;
import com.google.common.base.Preconditions;
import org.bukkit.Location;

public abstract class Arena extends Serializer.Object {

    private String name;
    private String displayName;
    private Location spawn;
    private Region region;

    /**
     * Create a new Arena instance.
     */
    protected Arena(String name, Location spawn) {
        this(name, spawn, null);
    }

    /**
     * Create a new Arena instance.
     */
    protected Arena(String name, Location spawn, Region region) {
        Preconditions.checkNotNull(name);
        Preconditions.checkNotNull(spawn);

        this.name = name;
        this.spawn = spawn;
        this.region = (region != null) ? region : new Region();
    }

    /**
     * Set the name of the arena.
     *
     * @param name The name you wish to set
     */
    public void setName(String name) {
        Preconditions.checkNotNull(name);

        this.name = name;
    }

    /**
     * Get the name of the arena.
     *
     * @return The name associated with the specified arena
     */
    public String getName() {
        return this.name;
    }

    /**
     * Set the arena's display name.
     *
     * @param displayName The arena's display name
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    /**
     * Get an arena's display name.
     *
     * @return The display name or original name
     */
    public String getDisplayName() {
        return (this.displayName != null) ? this.displayName : this.name;
    }

    /**
     * Set the arena's main spawn location
     *
     * @param spawn The spawn location
     */
    public void setSpawnLocation(Location spawn) {
        Preconditions.checkNotNull(spawn);

        this.spawn = spawn;
    }

    /**
     * Set the container of the arena.
     *
     * @param region
     */
    public void setRegion(Region region) {
        Preconditions.checkNotNull(region);

        this.region = region;
    }

    /**
     * Retrieve the arena container.
     *
     * @return
     */
    public Region getRegion() {
        return this.region;
    }

    /**
     * Get the arena's main spawn location.
     *
     * @return The main spawn location
     */
    public Location getSpawnLocation() {
        return this.spawn;
    }

    @Override
    public final boolean equals(Object object) {
        if (!(object instanceof Arena)) {
            return false;
        }
        return this.getName().equalsIgnoreCase(((Arena) object).getName());
    }

}
