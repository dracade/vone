package com.dracade.vone.bukkit.api.game;

import com.dracade.vone.bukkit.api.game.Arena;
import com.google.common.collect.ImmutableList;
import org.bukkit.event.Listener;

import java.util.UUID;

public interface Minigame extends Runnable, Listener {

    /**
     * Gets the delay that the task was scheduled to run after. A delay of 0
     * represents that the task started immediately.
     *
     * @return The delay (offset) in either milliseconds or ticks (ticks are
     *         exclusive to synchronous tasks)
     */
    long getDelay();

    /**
     * Gets the interval for repeating tasks. An interval of 0 represents that
     * the task does not repeat.
     *
     * @return The interval (period) in either milliseconds or ticks (ticks are
     *         exclusive to synchronous tasks)
     */
    long getInterval();

    /**
     * Get the arena that is is currently hosting the game.
     *
     * @return The arena that the player's are currently playing within
     */
    Arena getArena();

    /**
     * Gets the players in the minigame.
     *
     * @return A list of players in the minigame.
     */
    ImmutableList<UUID> getPlayers();

    /**
     * Get the id of the minigame.
     *
     * @return The unique id associated with the minigame
     */
    UUID getUniqueId();

}
