package com.dracade.vone.bukkit.api;

public final class VoneCore {

    private static VoneCore instance;

    /**
     * Retrieve the VoneCore instance.
     *
     * @return
     */
    public static VoneCore getInstance() {
        return (VoneCore.instance != null) ? (VoneCore.instance) : (VoneCore.instance = new VoneCore());
    }

    /**
     * Create a new VoneCore instance.
     */
    private VoneCore() {
        // TODO:
    }

}
