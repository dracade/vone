package com.dracade.vone.bukkit.api.serializer.adapters.bukkit;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import org.bukkit.Bukkit;
import org.bukkit.World;

import java.io.IOException;
import java.util.UUID;

public class WorldAdapter extends TypeAdapter<World> {

    @Override
    public void write(JsonWriter out, World value) throws IOException {
        if (value == null) {
            out.nullValue();
            return;
        }

        out.beginObject();
        out.name("name");
        out.value(value.getName());

        out.name("uniqueId");
        out.value(value.getUID().toString());
        out.endObject();

    }

    @Override
    public World read(JsonReader in) throws IOException {
        if (in.peek() == JsonToken.NULL) {
            return null;
        }

        in.beginObject();
        in.nextName();
        in.nextString();
        in.nextName();

        World world = Bukkit.getWorld(UUID.fromString(in.nextString()));

        in.endObject();

        return world;
    }

}