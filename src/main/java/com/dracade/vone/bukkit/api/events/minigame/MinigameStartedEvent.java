package com.dracade.vone.bukkit.api.events.minigame;

import com.dracade.vone.bukkit.api.game.Minigame;
import com.dracade.vone.bukkit.api.events.MinigameEvent;
import org.bukkit.event.HandlerList;

public class MinigameStartedEvent extends MinigameEvent {

    private static final HandlerList handlers = new HandlerList();

    /**
     * Retrieve the handlers associated with this event.
     *
     * @return A list of event handlers
     */
    public static HandlerList getHandlerList() {
        return MinigameStartedEvent.handlers;
    }

    /**
     * Create a new MinigameStartedEvent instance.
     *
     * @param minigame The minigame that is associated with the event
     */
    public MinigameStartedEvent(Minigame minigame) {
        super(minigame);
    }

    @Override
    public HandlerList getHandlers() {
        return MinigameStartedEvent.handlers;
    }

}
