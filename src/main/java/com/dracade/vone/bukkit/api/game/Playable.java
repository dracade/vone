package com.dracade.vone.bukkit.api.game;

import com.google.common.collect.ImmutableList;

import java.util.UUID;

public interface Playable {

    /**
     * Add a player to the joinable.
     *
     * @param uniqueId
     * @return
     */
    boolean addPlayer(UUID uniqueId);

    /**
     * Remove a player from the joinable.
     *
     * @param uniqueId
     * @return
     */
    boolean removePlayer(UUID uniqueId);

    /**
     * Check if the joinable contains the specified player.
     *
     * @param uniqueId
     * @return
     */
    boolean containsPlayer(UUID uniqueId);

    /**
     * Gets the players in the joinable.
     *
     * @return A list of players in the joinable.
     */
    ImmutableList<UUID> getPlayers();

}
