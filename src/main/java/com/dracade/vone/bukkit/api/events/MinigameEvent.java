package com.dracade.vone.bukkit.api.events;

import com.dracade.vone.bukkit.api.game.Minigame;
import org.bukkit.event.Event;

public abstract class MinigameEvent extends Event {

    private Minigame minigame;

    /**
     * Create a new MinigameEvent instance.
     *
     * @param minigame The minigame that is associated with the event
     */
    protected MinigameEvent(Minigame minigame) {
        this.minigame = minigame;
    }

    /**
     * Retrieve the associated minigame.
     *
     * @return Minigame instance
     */
    public Minigame getMinigame() {
        return this.minigame;
    }

}
