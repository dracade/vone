package com.dracade.vone.bukkit.api.serializer;

import com.dracade.vone.bukkit.api.serializer.adapters.bukkit.WorldAdapter;
import com.dracade.vone.bukkit.api.serializer.adapters.java.ClassAdapter;
import com.google.common.base.Preconditions;
import com.google.gson.*;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;

import java.io.FileWriter;
import java.io.IOException;
import java.io.File;
import java.nio.file.Files;
import java.util.Optional;
import java.util.logging.Level;

public class Serializer {

    private static Serializer instance;

    public static Serializer instance() {
        return (Serializer.instance != null) ? Serializer.instance : (Serializer.instance = new Serializer());
    }

    private GsonBuilder gsonBuilder;

    /**
     * Create a new Serializer instance.
     */
    private Serializer() {
        this.gsonBuilder = new GsonBuilder()
                .setPrettyPrinting()
                .registerTypeHierarchyAdapter(World.class, new WorldAdapter())
                .registerTypeHierarchyAdapter(Class.class, new ClassAdapter());
    }

    /**
     * Get the GsonBuilder.
     *
     * @return
     */
    public GsonBuilder getGsonBuilder() {
        return this.gsonBuilder;
    }

    /**
     * Save the object to the specified file.
     *
     * @return
     */
    public void toJson(Object object, File file) throws IOException {
        Preconditions.checkNotNull(object);
        Preconditions.checkNotNull(file);

        FileWriter writer = new FileWriter(file);
        this.gsonBuilder.create().toJson(object, writer);
        writer.flush();
        writer.close();
    }

    /**
     * Retrieve an object from a json file.
     *
     * @param file
     * @param type
     * @param <T>
     * @return
     */
    public <T extends Serializer.Object> Optional<T> fromJson(File file, Class<T> type) {
        Preconditions.checkNotNull(file);
        Preconditions.checkNotNull(type);

        try {
            String content = new String(Files.readAllBytes(file.toPath()));
            JsonParser parser = new JsonParser();

            JsonElement json = parser.parse(content);
            if (json.isJsonObject()) {
                JsonObject arenaType = json.getAsJsonObject().getAsJsonObject("meta");
                if (arenaType.isJsonObject()) {
                    String classType = arenaType.getAsJsonObject().get("class").getAsString();
                    Class<T> clazz = (Class<T>) Class.forName(classType);

                    return Optional.of(clazz.cast(this.gsonBuilder.create().fromJson(content, clazz)));
                } else {
                    Bukkit.getLogger().log(Level.SEVERE, ChatColor.RED + "[Invalid JSON]" + " " + file.getName() + ".");
                }
            } else {
                Bukkit.getConsoleSender().sendMessage(ChatColor.RED + "[Invalid JSON]" + " " + file.getName() + ".");
            }
        } catch (IOException e) {
            Bukkit.getConsoleSender().sendMessage(ChatColor.RED + "An error occurred whilst trying to read from " + file.getName() + ".");
        } catch (ClassNotFoundException e) {
            Bukkit.getConsoleSender().sendMessage(ChatColor.RED + "[Invalid JSON]" + " class not found from " + file.getName() + ".");
        }

        return Optional.empty();
    }

    /**
     * Add the class type to a serialized class.
     */
    public abstract static class Object {

        private final Class<? extends Object> meta;

        /**
         * Create a new Serializer.object
         */
        protected Object() {
            this.meta = this.getClass();
        }

    }
}
