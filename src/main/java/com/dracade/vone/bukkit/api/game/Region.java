package com.dracade.vone.bukkit.api.game;

import org.bukkit.Location;

import java.util.Optional;

public final class Region {

    private Location a, b;
    private boolean ignoreY;

    /**
     * Create a new Region instance.
     */
    public Region() {
        this(null, null);
    }

    /**
     * Create a new Region instance.
     *
     * @param a
     * @param b
     */
    public Region(Location a, Location b) {
        this.a = a;
        this.b = b;
        this.ignoreY = false;
    }

    /**
     * Set the A location.
     *
     * @param a
     */
    public void setA(Location a) {
        this.a = a;
    }

    /**
     * Get the A location.
     *
     * @return
     */
    public Optional<Location> getA() {
        return (this.a != null) ? Optional.of(this.a) : Optional.empty();
    }

    /**
     * Set the B location.
     *
     * @param b
     */
    public void setB(Location b) {
        this.b = b;
    }

    /**
     * Get the B location.
     *
     * @return
     */
    public Optional<Location> getB() {
        return (this.b != null) ? Optional.of(this.b) : Optional.empty();
    }

    /**
     * Set whether or not the container should ignore the Y axis when checking.
     *
     * @param ignoreY
     */
    public void setIgnoreY(boolean ignoreY) {
        this.ignoreY = ignoreY;
    }

    /**
     * Check whether or not the container is ignoring the Y axis.
     *
     * @return
     */
    public boolean isIgnoreY() {
        return this.ignoreY;
    }

    /**
     * Check if the specified location is within the container.
     *
     * @param c
     * @return
     */
    public boolean contains(Location c) {
        return this.contains(c, this.ignoreY);
    }

    /**
     * Check if the specified location is within the container.
     *
     * @param c
     * @return
     */
    public boolean contains(Location c, boolean ignoreY) {
        if (this.a == null || this.b == null) {
            return true;
        } else {
            if (c.getWorld().equals(this.a.getWorld()) && c.getWorld().equals(this.b.getWorld())) {

                int ax = Math.min(this.a.getBlockX(), this.b.getBlockX());
                int ay = Math.min(this.a.getBlockY(), this.b.getBlockY());
                int az = Math.min(this.a.getBlockZ(), this.b.getBlockZ());

                int bx = Math.max(this.a.getBlockX(), this.b.getBlockX());
                int by = Math.max(this.a.getBlockY(), this.b.getBlockY());
                int bz = Math.max(this.a.getBlockZ(), this.b.getBlockZ());

                Location aa = new Location(this.a.getWorld(), ax, ay, az);
                Location bb = new Location(this.b.getWorld(), bx, by, bz);

                boolean checkX = c.getBlockX() >= aa.getBlockX() && c.getBlockX() <= bb.getBlockX();
                boolean checkY = c.getBlockY() >= aa.getBlockY() && c.getBlockY() <= bb.getBlockY();
                boolean checkZ = c.getBlockZ() >= aa.getBlockZ() && c.getBlockZ() <= bb.getBlockZ();

                return checkX && (ignoreY || checkY) && checkZ;
            } else {
                return false;
            }
        }
    }

    /**
     * Remove all of the specified entity types.
     *
     * @param types
     */
    public void removeEntities(Class<?>... types) {
        Optional<Location> location = this.getA();
        if (!location.isPresent()) {
            location = this.getB();
        }

        if (location.isPresent()) {
            location.get().getWorld().getEntitiesByClasses(types).stream().forEach(entity -> {
                if (this.contains(entity.getLocation())) {
                    entity.remove();
                }
            });
        }
    }


}
