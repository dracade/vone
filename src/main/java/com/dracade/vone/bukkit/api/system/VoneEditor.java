package com.dracade.vone.bukkit.api.system;

import com.dracade.vone.bukkit.api.game.Arena;
import com.google.common.collect.ImmutableSet;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

public interface VoneEditor {

    /**
     * Return the type of arena that the editor handles.
     *
     * @param <T>
     * @return
     */
    <T extends Arena> Class<T> getArenaType();

    /**
     * Retrieve the executor's usage.
     *
     * @return The sub-command's usage
     */
    ImmutableSet<String> getCreateExecutorUsage();

    /**
     * Retrieve the executor's usage.
     *
     * @return The sub-command's usage
     */
    ImmutableSet<String> getEditExecutorUsage();

    /**
     * Retrieve the executor's usage.
     *
     * @return The sub-command's usage
     */
    ImmutableSet<String> getInfoExecutorUsage();

    /**
     * Executes on the create sub command.
     *
     * @param player
     * @param args
     * @return
     * @throws ArrayIndexOutOfBoundsException
     */
    Optional<Arena> onCreateExecutor(Player player, String[] args) throws ArrayIndexOutOfBoundsException;

    /**
     * Executes on the edit sub command.
     *
     * @param player
     * @param args
     * @return
     * @throws ArrayIndexOutOfBoundsException
     */
    boolean onEditExecutor(Player player, Arena arena, String[] args) throws ArrayIndexOutOfBoundsException;

    /**
     * Executes on the save sub command.
     *
     * @param player
     * @param arena
     * @param args
     * @return
     * @throws ArrayIndexOutOfBoundsException
     */
    Optional<Arena> onSaveExecutor(Player player, Arena arena, String[] args) throws ArrayIndexOutOfBoundsException;

    /**
     * Executes on the info sub command.
     *
     * @param player
     * @param args
     * @return
     * @throws ArrayIndexOutOfBoundsException
     */
    boolean onInfoExecutor(Player player, Arena arena, String[] args) throws ArrayIndexOutOfBoundsException;

    /**
     * ArenaCommandEditor handler.
     */
    final class Handler {

        private static Map<UUID, Arena> editors = new HashMap<UUID, Arena>();

        /**
         * Create an arena builder instance.
         *
         * @param uuid
         * @param arena
         */
        public static void create(UUID uuid, Arena arena) {
            VoneEditor.Handler.editors.put(uuid, arena);
        }

        /**
         * Get the player's arena builder instance.
         *
         * @param uuid
         * @return
         */
        public static Optional<Arena> get(UUID uuid) {
            Arena builder = VoneEditor.Handler.editors.get(uuid);
            return (builder != null) ? Optional.of(builder) : Optional.empty();
        }

        /**
         * Destroy the player's arena builder instance.
         * @param uuid
         * @return
         */
        public static boolean destroy(UUID uuid) {
            return (VoneEditor.Handler.editors.remove(uuid) != null);
        }

        /**
         * Check if the player has an arena builder instance.
         *
         * @param uuid
         * @return
         */
        public static boolean has(UUID uuid) {
            return VoneEditor.Handler.editors.containsKey(uuid);
        }

    }

}
