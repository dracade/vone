package com.dracade.vone.bukkit.api.game;

import org.bukkit.entity.Player;

public interface Menu {

    /**
     * Open the inventory menu for the player.
     *
     * @param player
     */
    void open(Player player);

    /**
     * Open the menu but pass the old menu through as a parameter.
     *
     * @param player
     * @param lastMenu
     */
    void open(Player player, Menu lastMenu);

}
