package com.dracade.vone.bukkit.api.exceptions;

import com.dracade.vone.bukkit.api.game.Arena;

public class ArenaActiveException extends Exception {

    private Arena arena;

    /**
     * ArenaActiveException
     *
     * @param arena
     */
    public ArenaActiveException(Arena arena) {
        this.arena = arena;
    }

    /**
     * Retrieve the already active arena.
     *
     * @return
     */
    public Arena getArena() {
        return this.arena;
    }

}
