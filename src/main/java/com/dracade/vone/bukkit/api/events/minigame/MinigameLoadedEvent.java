package com.dracade.vone.bukkit.api.events.minigame;

import com.dracade.vone.bukkit.api.events.MinigameEvent;
import com.dracade.vone.bukkit.api.game.Minigame;
import org.bukkit.event.HandlerList;

public class MinigameLoadedEvent extends MinigameEvent {

    private static final HandlerList handlers = new HandlerList();

    /**
     * Retrieve the handlers associated with this event.
     *
     * @return A list of event handlers
     */
    public static HandlerList getHandlerList() {
        return MinigameLoadedEvent.handlers;
    }

    /**
     * Create a new MinigameLoadedEvent instance.
     *
     * @param minigame The minigame that is associated with the event
     */
    public MinigameLoadedEvent(Minigame minigame) {
        super(minigame);
    }

    @Override
    public HandlerList getHandlers() {
        return MinigameLoadedEvent.handlers;
    }

}
