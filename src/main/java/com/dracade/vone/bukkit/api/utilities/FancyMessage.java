package com.dracade.vone.bukkit.api.utilities;

import org.bukkit.ChatColor;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class FancyMessage extends mkremins.fanciful.FancyMessage {

    /**
     * Create a new FancyMessage.
     */
    public FancyMessage() {
        this("");
    }

    /**
     * Create a new FancyMessage.
     */
    public FancyMessage(String message) {
        super(message);
    }

    /**
     * Create a FancyMessage with placeholders.
     */
    public static class PlaceholderMessage {

        private String message;
        private Map<String, Placeholder> placeholders;

        /**
         * Create a FancyMessage with placeholders.
         *
         * @param message
         */
        public PlaceholderMessage(String message) {
            this.message = message;
            this.placeholders = new HashMap<String, Placeholder>();
        }

        /**
         * Add a placeholder to the FancyMessage.
         *
         * @param replaceable
         * @param placeholder
         */
        public PlaceholderMessage placeholder(String replaceable, Placeholder placeholder) {
            this.placeholders.put(replaceable, placeholder);
            return this;
        }

        /**
         * Build the FancyMessage instance.
         * @return
         */
        public FancyMessage build() {
            FancyMessage message = new FancyMessage();
            ChatColor color = ChatColor.WHITE;

            for (String messagePart : this.message.split("\\s+")) {
                if (messagePart.contains(String.valueOf(ChatColor.COLOR_CHAR))) {
                    color = ChatColor.getByChar(messagePart.charAt(messagePart.indexOf(ChatColor.COLOR_CHAR) + 1));
                }

                boolean replaced = false;
                for (String replaceable : this.placeholders.keySet()) {
                    if (messagePart.contains(replaceable)) {
                        this.placeholders.get(replaceable).onReplace(message, color);

                        replaced = true;
                    }
                }

                if (!replaced) {
                    message.then(messagePart).color(color);
                }

                // Add the space between each string
                message.then(" ").color(color);
            }

            return message;
        }

        /**
         * Placeholder
         */
        public interface Placeholder {

            /**
             * This method is called once
             *
             * @param placeholder The placeholder textual component
             * @param color The last used color
             */
            void onReplace(FancyMessage placeholder, ChatColor color);

        }

    }

}