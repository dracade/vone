package com.dracade.vone.bukkit.api.events.queue;

import com.dracade.vone.bukkit.api.events.QueueEvent;
import com.dracade.vone.bukkit.api.system.VoneQueue;
import com.dracade.vone.bukkit.api.system.VoneQueueItem;
import org.bukkit.event.HandlerList;

public class QueueAddEvent extends QueueEvent {

    private static final HandlerList handlers = new HandlerList();

    /**
     * Retrieve the handlers associated with this event.
     *
     * @return A list of event handlers
     */
    public static HandlerList getHandlerList() {
        return QueueAddEvent.handlers;
    }

    /**
     * Create a new QueueAddEvent instance.
     *
     * @param queue The queue that is associated with the event
     * @param item  The queue iteme that is associated with the event
     */
    public QueueAddEvent(VoneQueue queue, VoneQueueItem item) {
        super(queue, item);
    }

    @Override
    public HandlerList getHandlers() {
        return QueueAddEvent.handlers;
    }

}
