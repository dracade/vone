package com.dracade.vone.bukkit.api.events.queue;

import com.dracade.vone.bukkit.api.events.QueueEvent;
import com.dracade.vone.bukkit.api.system.VoneQueue;
import com.dracade.vone.bukkit.api.system.VoneQueueItem;
import org.bukkit.event.HandlerList;

public class QueueRemoveEvent extends QueueEvent {

    private static final HandlerList handlers = new HandlerList();

    /**
     * Retrieve the handlers associated with this event.
     *
     * @return A list of event handlers
     */
    public static HandlerList getHandlerList() {
        return QueueRemoveEvent.handlers;
    }

    private boolean polled;

    /**
     * Create a new QueueRemoveEvent instance.
     *
     * @param queue The queue that is associated with the event
     * @param item  The queue iteme that is associated with the event
     */
    public QueueRemoveEvent(VoneQueue queue, VoneQueueItem item) {
        super(queue, item);
        this.polled = false;
    }

    /**
     * Create a new QueueRemoveEvent instance.
     *
     * @param queue The queue that is associated with the event
     * @param item  The queue iteme that is associated with the event
     */
    public QueueRemoveEvent(VoneQueue queue, VoneQueueItem item, boolean polled) {
        super(queue, item);
        this.polled = polled;
    }

    /**
     * Check if the entity was removed due to being
     * first in the queue.
     *
     * @return
     */
    public boolean wasPolled() {
        return this.polled;
    }

    @Override
    public HandlerList getHandlers() {
        return QueueRemoveEvent.handlers;
    }

}
