package com.dracade.vone.bukkit.api.system;

import com.dracade.vone.bukkit.VoneBukkit;
import com.dracade.vone.bukkit.api.game.Arena;
import com.dracade.vone.bukkit.api.game.Minigame;
import com.dracade.vone.bukkit.api.events.minigame.MinigameStartedEvent;
import com.dracade.vone.bukkit.api.events.minigame.MinigameStoppedEvent;
import com.dracade.vone.bukkit.api.exceptions.ArenaActiveException;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;

import java.io.*;
import java.nio.file.FileAlreadyExistsException;
import java.util.*;
import java.util.stream.Collectors;

public final class VoneRegistry {

    private VoneBukkit vone;
    private File arenasDirectory;
    private Map<Minigame, Integer> minigames;

    /**
     * Create a new VoneRegistry instance.
     *
     * @param arenasDirectory
     */
    public VoneRegistry(VoneBukkit vone, File arenasDirectory) {
        this.vone = vone;
        this.arenasDirectory = arenasDirectory;
        this.minigames = new HashMap<Minigame, Integer>();
    }


    /**
     * Register a minigame.
     *
     * @param minigame The minigame you wish to register
     * @return False if the minigame is already registered
     */
    public final boolean register(Minigame minigame) throws ArenaActiveException {
        return this.register(minigame, true);
    }

    /**
     * Register a minigame.
     *
     * @param minigame The minigame you wish to register
     * @param task Whether or not to schedule a repeating task
     * @return False if the minigame is already registered
     */
    public final boolean register(Minigame minigame, boolean task) throws ArenaActiveException {
        Preconditions.checkNotNull(minigame);

        if (!this.isActive(minigame.getArena())) {
            if (!this.isRegistered(minigame)) {
                int worker = -1;

                // Schedule the minigame if it's a task.
                if (task) {
                    worker = Bukkit.getScheduler().scheduleSyncRepeatingTask(this.vone, minigame, minigame.getDelay(), minigame.getInterval());
                }

                // Register it within VoneBukkit.
                this.minigames.put(minigame, worker);

                // Register the minigame to the event bus so that it may listen to events.
                Bukkit.getPluginManager().registerEvents(minigame, this.vone);

                // Call the event so that minigames can start.
                Bukkit.getPluginManager().callEvent(new MinigameStartedEvent(minigame));

                return true;
            }
        } else {
            throw new ArenaActiveException(minigame.getArena());
        }

        return false;
    }

    /**
     * Unregister a minigame.
     *
     * @param minigame The minigame you wish to unregister
     * @return False if the minigame isn't registered
     */
    public final boolean unregister(Minigame minigame) {
        Preconditions.checkNotNull(minigame);

        for(Map.Entry<Minigame, Integer> entry : this.minigames.entrySet()) {
            if (entry.getKey().getUniqueId() == minigame.getUniqueId()) {
                // Stop the task if it is still scheduled.
                Bukkit.getScheduler().cancelTask(entry.getValue());

                // Call the event so that minigames can stop.
                Bukkit.getPluginManager().callEvent(new MinigameStoppedEvent(minigame));

                // Unregister the minigame from the event bus so that it isn't persisted within memory.
                HandlerList.unregisterAll(minigame);

                // Unregister it from VoneBukkit.
                this.minigames.remove(entry.getKey());

                return true;
            }
        }
        return false;
    }

    /**
     * Determine if a minigame is registered.
     *
     * @param minigame The minigame you wish to check
     * @return True if the minigame has been registered
     */
    public final boolean isRegistered(Minigame minigame) {
        Preconditions.checkNotNull(minigame);

        return this.minigames.entrySet().stream()
                .anyMatch(m -> m.getKey().getUniqueId().equals(minigame.getUniqueId()));
    }

    /**
     * Retrieve a minigame instance by it's unique id.
     *
     * @param uniqueId The id of the minigame
     * @return An empty optional wrapper if the minigame could not be found
     */
    public final Optional<Minigame> getMinigame(UUID uniqueId) {
        Optional<Map.Entry<Minigame, Integer>> find = this.minigames.entrySet().stream()
                .filter(m -> m.getKey().getUniqueId().equals(uniqueId))
                .findFirst();

        if (find.isPresent()) {
            return Optional.of(find.get().getKey());
        } else {
            return Optional.empty();
        }
    }

    /**
     * Get the host minigame of the specified arena.
     *
     * @param arena The arena that holds the minigame
     * @return An empty optional wrapper if the minigame could not be found
     */
    public final Optional<Minigame> getMinigame(Arena arena) {
        Optional<Map.Entry<Minigame, Integer>> find = this.minigames.entrySet().stream()
                .filter(m -> m.getKey().getArena().equals(arena))
                .findFirst();

        if (find.isPresent()) {
            return Optional.of(find.get().getKey());
        } else {
            return Optional.empty();
        }
    }

    /**
     * Get the host minigame where the player is active.
     *
     * @param player
     * @return An empty optional wrapper if the player isn't within a minigame
     */
    public Optional<Minigame> getMinigame(Player player) {
        return this.getMinigames().stream().filter(m -> m.getPlayers().contains(player.getUniqueId())).findFirst();
    }

    /**
     * Retrieve all of the registered minigames.
     *
     * @return A list of all of the currently active minigames
     */
    public final ImmutableList<Minigame> getMinigames() {
        return ImmutableList.copyOf(this.minigames.keySet());
    }


    /**
     * Save an arena.
     *
     * @param arena
     * @return true if the arena was saved
     */
    public final void save(Arena arena) throws FileAlreadyExistsException {
        this.save(arena, false);
    }

    /**
     * Save an arena.
     *
     * @param arena
     * @param force save
     * @return true if the arena was saved
     */
    public final void save(Arena arena, boolean force) throws FileAlreadyExistsException {
        Preconditions.checkNotNull(arena);

        String fileName = arena.getName().toLowerCase() + ".json";
        File dest = new File(this.arenasDirectory, fileName);
        if (dest.exists() && !force) {
            throw new FileAlreadyExistsException(fileName);
        } else {
            if (dest.exists()) {
                dest.delete();
            }

            try {
                dest.createNewFile();
                this.vone.serializer().toJson(arena, dest);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Delete arena.
     *
     * @param arena
     */
    public final void delete(Arena arena) {
        Preconditions.checkNotNull(arena);

        String fileName = arena.getName().toLowerCase() + ".json";
        File dest = new File(this.arenasDirectory, fileName);
        if (dest.exists()) {
            dest.delete();
        }
    }

    /**
     * Retrieve all of the arenas.
     *
     * @return A list of all of the arenas
     */
    public final ImmutableList<Arena> getArenas() {
        Set<Arena> arenas = new HashSet<Arena>();

        for (File f : this.arenasDirectory.listFiles()) {
            Optional<Arena> a = this.vone.serializer().fromJson(f, Arena.class);
            if (a.isPresent()) {
                arenas.add(a.get());
            }
        }

        return ImmutableList.copyOf(arenas);
    }

    /**
     * Retrieve all of the active arenas.
     *
     * @return A list of all of the active arenas
     */
    public final ImmutableList<Arena> getActiveArenas() {
        return ImmutableList.copyOf(this.getArenas().stream().filter(a -> this.isActive(a)).collect(Collectors.toList()));
    }

    /**
     * Retrieve all of the inactive arenas.
     *
     * @return A list of all of the inactive arenas
     */
    public final ImmutableList<Arena> getInactiveArenas() {
        return ImmutableList.copyOf(this.getArenas().stream().filter(a -> !this.isActive(a)).collect(Collectors.toList()));
    }

    /**
     * Determine if an arena is currently active.
     *
     * @param arena The arena you wish to check
     * @return True if the arena is currently being used
     */
    public final boolean isActive(Arena arena) {
        return this.getMinigames().stream()
                .anyMatch(m -> m.getArena().equals(arena));
    }

    /**
     * Determine if a player is already within a minigame.
     *
     * @param player The player you wish to check
     * @return
     */
    public final boolean isActive(Player player) {
        Preconditions.checkNotNull(player);

        return this.getMinigames().stream().anyMatch(m -> m.getPlayers().contains(player.getUniqueId()));
    }

}
