package com.dracade.vone.bukkit.api.system;

import org.bukkit.command.CommandSender;

public interface VoneExecutor {

    /**
     * Execute the command and check for sub arguments.
     *
     * @param sender The sender of the command
     * @param args The command arguments
     * @return True if the command was executed successfully
     */
    boolean execute(CommandSender sender, String[] args) throws ArrayIndexOutOfBoundsException;

}
