package com.dracade.vone.bukkit.api.utilities;

import com.dracade.vone.bukkit.VoneBukkit;
import com.google.common.collect.ImmutableList;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.*;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;

import java.util.*;

public class FancyMenu {

    private Inventory inventory;
    private Map<Integer, Runnable> actions;

    /**
     * Create a new FancyMenu instance.
     *
     * @param inventory
     */
    public FancyMenu(Inventory inventory) {
        this.inventory = inventory;
        this.actions = new HashMap<Integer, Runnable>();
    }

    /**
     * Retrieve the inventory associated with this
     * action list.
     *
     * @return
     */
    public final Inventory getInventory() {
        return this.inventory;
    }

    /**
     * Set the action for a specific menu slot.
     *
     * @param slot
     * @param action
     */
    public final FancyMenu setAction(int slot, Runnable action) {
        this.actions.put(slot, action);
        return this;
    }

    /**
     * Retrieve the menu action for a specific slot.
     *
     * @param slot
     * @return
     */
    public final Optional<Runnable> getAction(int slot) {
        Runnable action = this.actions.get(slot);
        return (action != null) ? Optional.of(action) : Optional.empty();
    }


    /**
     * Retrieve all of the menu actions.
     *
     * @return
     */
    public final List<Map.Entry<Integer, Runnable>> getActions() {
        return ImmutableList.copyOf(this.actions.entrySet());
    }

    /**
     * Open the inventory menu
     */
    public void open(Player player) {
        Inventory inventory = this.getInventory();

        if (inventory != null) {
            /**
             * Register a listener to listen for the
             * inventory specific events.
             *
             * The listener should be unregistered if the player
             * quits the game or closes the inventory.
             */
            Bukkit.getPluginManager().registerEvents(new Listener() {

                @EventHandler
                private void onPlayerQuit(PlayerQuitEvent event) {
                    if (event.getPlayer().getUniqueId().equals(player.getUniqueId())) {
                        HandlerList.unregisterAll(this);
                    }
                }

                @EventHandler
                private void onInventoryClose(InventoryCloseEvent event) {
                    if (event.getInventory().equals(inventory)) {
                        HandlerList.unregisterAll(this);
                    }
                }

                @EventHandler
                private void onInventoryOpen(InventoryMoveItemEvent event) {
                    if (event.getSource().equals(inventory) || event.getDestination().equals(inventory)) {
                        event.setCancelled(true);
                    }
                }

                @EventHandler
                private void onInventoryClick(InventoryClickEvent event) {
                    if (event.getInventory().equals(inventory)) {
                        if (event.getWhoClicked() instanceof Player) {
                            Player clicker = (Player) event.getWhoClicked();
                            if (clicker.getUniqueId().equals(player.getUniqueId())) {
                                Optional<Runnable> action = FancyMenu.this.getAction(event.getRawSlot());
                                if (action.isPresent()) {
                                    action.get().run();
                                    event.setCancelled(true);
                                }
                            }
                        }
                    }
                }

            }, VoneBukkit.plugin());

            /**
             * Open the inventory for the player.
             */
            player.openInventory(inventory);
        } else {
            throw new NullPointerException();
        }
    }

}
