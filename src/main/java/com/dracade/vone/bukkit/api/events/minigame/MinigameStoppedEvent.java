package com.dracade.vone.bukkit.api.events.minigame;

import com.dracade.vone.bukkit.api.game.Minigame;
import com.dracade.vone.bukkit.api.events.MinigameEvent;
import org.bukkit.event.HandlerList;

public class MinigameStoppedEvent extends MinigameEvent {

    private static final HandlerList handlers = new HandlerList();

    /**
     * Retrieve the handlers associated with this event.
     *
     * @return A list of event handlers
     */
    public static HandlerList getHandlerList() {
        return MinigameStoppedEvent.handlers;
    }

    /**
     * Create a new MinigameStoppedEvent instance.
     *
     * @param minigame The minigame that is associated with the event
     */
    public MinigameStoppedEvent(Minigame minigame) {
        super(minigame);
    }

    @Override
    public HandlerList getHandlers() {
        return MinigameStoppedEvent.handlers;
    }

}
