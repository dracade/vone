package com.dracade.vone.bukkit;

import com.blackypaw.mc.i18n.*;
import com.comphenix.protocol.ProtocolLibrary;
import com.dracade.vone.bukkit.api.VoneCore;
import com.dracade.vone.bukkit.api.serializer.Serializer;
import com.dracade.vone.bukkit.plugin.utilities.PlayerLocaleResolver;
import com.dracade.vone.bukkit.plugin.VoneCommand;
import com.google.common.base.Preconditions;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;

import com.dracade.vone.bukkit.api.system.VoneRegistry;

import java.io.*;
import java.util.*;

/**
 * TODO: refactor this class
 * ...
 * move the arena/minigame stuff out into the API
 */
public class VoneBukkit extends JavaPlugin {

    private static VoneBukkit instance;

    /**
     * Retrieve the vone plugin.
     *
     * @return VoneBukkit instance
     */
    public static VoneBukkit plugin() {
        return VoneBukkit.instance;
    }

    private Localizer localizer;
    private VoneRegistry registry;
    private Map<UUID, Locale> i18n;
    private File translationsDirectory, arenasDirectory;
    private VoneCore core;

    /**
     * Create a new VoneBukkit instance.
     */
    public VoneBukkit() throws InstantiationException {
        if (VoneBukkit.instance != null) {
            throw new InstantiationException("VoneBukkit is a singleton instance.");
        } else {
            /**
             * Instantiate the plugin
             */
            VoneBukkit.instance = this;

            this.i18n = new HashMap<UUID, Locale>();
            this.core = VoneCore.getInstance();
        }
    }

    @Override
    public final void onEnable() {
        PlayerLocaleResolver resolver = new PlayerLocaleResolver();

        ProtocolLibrary.getProtocolManager().addPacketListener(resolver);
        I18NUtilities.setLocaleResolver(resolver);

        Bukkit.getPluginManager().registerEvents(new Listener() {
            @EventHandler
            private void onPlayerQuit(PlayerQuitEvent event) {
                VoneBukkit.this.setPlayerLocale(event.getPlayer(), null);
            }
        }, this);

        this.getPluginLoader();

        this.arenasDirectory = new File(this.getDataFolder(), "arenas");
        this.arenasDirectory.mkdirs();

        /**
         * Load translations
         */
        this.translationsDirectory = new File(this.getDataFolder(), "translations");
        this.translationsDirectory.mkdirs();
        if (!translationsDirectory.exists() || !new File(this.translationsDirectory, "en.yml").exists()) {
            InputStream defaultLocale = this.getResource("en.yml");
            OutputStream out;
            try {
                out = new FileOutputStream(new File(this.translationsDirectory, "en.yml"));
                byte[] buf = new byte[1024];
                int len;
                while((len = defaultLocale.read(buf)) > 0){
                    out.write(buf, 0, len);
                }
                out.close();
                defaultLocale.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        TranslationStorage translations = new YamlTranslationStorage(this.translationsDirectory);

        try {
            for (File file : this.translationsDirectory.listFiles()) {
                if (file.getName().endsWith(".yml")) {
                    String locale = file.getName().replace(".yml", "");
                    translations.loadLanguage(new Locale(locale));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        this.localizer = I18NUtilities.createLocalizer(translations);
        this.registry = new VoneRegistry(this, this.arenasDirectory);
        this.construct();
    }

    @Override
    public final void onDisable() {
        this.registry.getMinigames().stream().forEach(minigame -> {
            this.registry.unregister(minigame);
        });

        this.deconstruct();
    }

    /**
     * This method will be called once VoneBukkit has been enabled.
     */
    private void construct() {
        this.getCommand(this.getDescription().getName()).setExecutor(VoneCommand.instance());
    }

    /**
     * This method will be called once VoneBukkit has been disabled.
     */
    private void deconstruct() {
        return;
    }

    /**
     * Retrieve the localizer.
     *
     * @return Localizer
     */
    public Localizer i18n() {
        return this.localizer;
    }

    /**
     * Retrieve the registry.
     *
     * @return Registry
     */
    public VoneRegistry registry() {
        return this.registry;
    }

    /**
     * Serializer instance.
     *
     * @return Serializer
     */
    public Serializer serializer() {
        return Serializer.instance();
    }

    /**
     * Get the default locale.
     *
     * @return The default locale.
     */
    public final Locale getDefaultLocale() {
        return I18NUtilities.getFallbackLocale();
    }

    /**
     * Set a player's locale preference.
     *
     * @param player The player's preference you wish to set
     * @param locale The player's locale preference
     */
    public final void setPlayerLocale(Player player, Locale locale) {
        Preconditions.checkNotNull(player);

        if (locale != null) {
            this.i18n.put(player.getUniqueId(), locale);
        } else {
            this.i18n.remove(player.getUniqueId());
        }
    }

    /**
     * Get a player's locale preference.
     *
     * @param player The player's preference you wish to retrieve
     * @return The player's locale preference
     */
    public final Locale getPlayerLocale(Player player) {
        Preconditions.checkNotNull(player);

        return this.i18n.containsKey(player.getUniqueId()) ? this.i18n.get(player.getUniqueId()) : I18NUtilities.getFallbackLocale();
    }

    /**
     * Retrieve the core helper methods for vone.
     *
     * @return
     */
    public final VoneCore getCore() {
        return this.core;
    }

    /**
     * Send plugin information.
     *
     * @param plugin
     * @param localizer
     * @param sender
     */
    public final boolean sendPluginInformation(JavaPlugin plugin, Localizer localizer, CommandSender sender) {
        Preconditions.checkNotNull(plugin);
        Preconditions.checkNotNull(localizer);
        Preconditions.checkNotNull(sender);

        final Locale locale;

        if (sender instanceof Player) {
            locale = this.getPlayerLocale((Player) sender);
        } else {
            locale = this.getDefaultLocale();
        }

        String pluginName = localizer.translateDirect(locale, "settings.plugin-info.name")
                .replace("{{PLUGIN_NAME}}", plugin.getDescription().getName());

        String pluginVersion = localizer.translateDirect(locale, "settings.plugin-info.version")
                .replace("{{PLUGIN_VERSION}}", plugin.getDescription().getVersion());

        String pluginDescription = localizer.translateDirect(locale, "settings.plugin-info.description");

        String developedBy = localizer.translateDirect(locale, "settings.plugin-info.developer")
                .replace("{{DEVELOPER}}", plugin.getDescription().getAuthors().get(0));

        if (!(pluginName.isEmpty() && pluginVersion.isEmpty() && pluginDescription.isEmpty() && developedBy.isEmpty())) {
            sender.sendMessage("");
            if (!pluginName.isEmpty()) sender.sendMessage("  " + pluginName);
            if (!pluginVersion.isEmpty()) sender.sendMessage("  " + pluginVersion);
            if (!pluginDescription.isEmpty()) sender.sendMessage("  " + pluginDescription);
            if (!developedBy.isEmpty()) sender.sendMessage("  " + developedBy);
            sender.sendMessage("");

            return true;
        } else {
            return false;
        }
    }

}
