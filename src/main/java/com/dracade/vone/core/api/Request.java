package com.dracade.vone.core.api;

import com.google.common.base.Preconditions;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.RemovalListener;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

public interface Request {

    /**
     *  This method is called once the request has been created.
     *
     * @param id The id of the request
     */
    void onCreate(UUID id);

    /**
     * This method is called once the request is accepted.
     */
    void onAccept();

    /**
     * This method is called once the request is declined.
     */
    void onDecline();

    /**
     * This method is called once the request has expired.
     */
    default void onExpire() {
        return;
    }

    /**
     * Request handler
     */
    final class Handler {

        private static Cache<UUID, Request> requests = CacheBuilder.newBuilder()
                .expireAfterWrite(1, TimeUnit.MINUTES)
                .removalListener((RemovalListener<UUID, Request>) notification -> {
                    if (notification.getValue() != null) {
                        notification.getValue().onExpire();
                    }
                })
                .build();

        /**
         * Create a uuid for the request.
         *
         * @param request
         * @return
         */
        public static UUID create(Request request) {
            Preconditions.checkNotNull(request);

            UUID id = UUID.randomUUID();
            Handler.requests.put(id, request);

            request.onCreate(id);

            return id;
        }

        /**
         * Accept a request.
         *
         * @param id The id of the request
         */
        public static void accept(UUID id) {
            Request r = Handler.requests.getIfPresent(id);
            if (r != null) {
                r.onAccept();
            }
            Handler.requests.invalidate(id);
        }

        /**
         * Decline a request.
         *
         * @param id The id of the request
         */
        public static void decline(UUID id) {
            Request r = Handler.requests.getIfPresent(id);
            if (r != null) {
                r.onDecline();
            }
            Handler.requests.invalidate(id);
        }

        /**
         * Check if request exists by id
         *
         * @param id
         * @return
         */
        public static boolean has(UUID id) {
            return (Handler.requests.getIfPresent(id) != null);
        }

    }

}
